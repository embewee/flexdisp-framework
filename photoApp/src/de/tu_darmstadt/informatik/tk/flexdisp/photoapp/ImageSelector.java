package de.tu_darmstadt.informatik.tk.flexdisp.photoapp;

import java.util.List;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;

public class ImageSelector {
	
	private List<Image> images;
	private Image frameImage;
	private int currentIndex;
	private double fitWidth;
	private double fitHeight;

	public ImageSelector(List<Image> images, Image frameImage, double fitWidth, double fitHeight) {
		this.images = images;
		this.fitWidth = fitWidth;
		this.fitHeight = fitHeight;
		this.frameImage = frameImage;
		currentIndex = 0;
	}
	
	public void next() {
		timeMachineChangeCurrent(+1);
	}

	public void previous() {
		timeMachineChangeCurrent(-1);
	}

	private void timeMachineChangeCurrent(int direction) {
		if (!images.isEmpty()) {
			currentIndex = circleIndex(direction);
		}
	}
	
	private int circleIndex(int value) {
		return ((currentIndex + value) % images.size() 
				+ images.size()) % images.size();
	}

	public Pane getFrame() {
		StackPane pane = new StackPane();
		ImageView frameView = new ImageView(frameImage);
		ImageView pictureView = new ImageView(images.get(currentIndex));
		pictureView.setFitWidth(fitWidth);
		pictureView.setFitHeight(fitHeight);
		pane.getChildren().addAll(frameView, pictureView);
		return pane;
	}
	
	public Image getImage() {
		return images.get(currentIndex);
	}
}

