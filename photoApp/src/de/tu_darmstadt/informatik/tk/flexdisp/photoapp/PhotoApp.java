package de.tu_darmstadt.informatik.tk.flexdisp.photoapp;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.List;

import javafx.animation.FadeTransition;
import javafx.animation.ParallelTransition;
import javafx.animation.ScaleTransition;
import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.util.Duration;
import de.tu_darmstadt.informatik.tk.flexdisp.deformationsensor.sensor.DeformationEvent;
import de.tu_darmstadt.informatik.tk.flexdisp.deformationsensor.sensor.DeformationEventListener;
import de.tu_darmstadt.informatik.tk.flexdisp.launchable.Context;
import de.tu_darmstadt.informatik.tk.flexdisp.launchable.Launchable;
import de.tu_darmstadt.informatik.tk.flexdisp.launchable.utils.SchedulerUtils;
import de.tu_darmstadt.informatik.tk.flexkit.jconnector.OperationMode;
import de.tu_darmstadt.informatik.tk.flexkit.jconnector.StageSnapshoter;

/**
 * Entry point of the photo applet. Displays the picture selector. 
 * @author Michael Winkler
 *
 */
public class PhotoApp implements Launchable {
	
	private StackPane appPane;
	private Context context;
	private ImageSelector imageSelector;
	
	private Pane currentImagePane;
	
	private int BLOCK_TIME = 500;
	
	private DeformationEventListener discreteListener;
	private DeformationEventListener continuousListener;
	
	private boolean scrollingBlocked = false;
	
	/**
	 * @param photoDirectory File path, where images (.jpg) are located in
	 */
	public PhotoApp(File photoDirectory) {
		appPane = new StackPane();
		appPane.setAlignment(Pos.CENTER);
		
		Image backgroundImage = new Image(getClass().getResourceAsStream("bg.jpg"));
		appPane.setBackground(new Background(new BackgroundImage(backgroundImage, 
				BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, BackgroundSize.DEFAULT)));
		
		Image frameImage = new Image(getClass().getResourceAsStream("frame.png"));
		
		List<Image> images = findImages(photoDirectory);
		double fitWidth = frameImage.getWidth() - 2 * 77; 
		double fitHeight = frameImage.getHeight() - 2 * 77; 
		imageSelector = new ImageSelector(images, frameImage, fitWidth, fitHeight);
		
		Pane picturePane = imageSelector.getFrame();
		currentImagePane = picturePane;
		currentImagePane.setOnMouseClicked(e -> {
			pictureSelected();
		});
		appPane.getChildren().add(picturePane);
		
		appPane.setOnScroll(event -> {
			int sign = (int) Math.signum(event.getDeltaY());
			if (sign > 0) {
				nextImage();
			} else if (sign < 0) {
				previousImage();
			}
		});
	}

	private void nextImage() {
		imageSelector.next();
		Pane comingImage = imageSelector.getFrame();
		swapBackground(currentImagePane, comingImage);
	}
	
	private void previousImage() {
		imageSelector.previous();
		Pane comingImage = imageSelector.getFrame();
		swapForeground(currentImagePane, comingImage);
	}
	
	private void pictureSelected() {
		PhotoEditor photoEditor = new PhotoEditor(imageSelector.getImage());
		context.launch(photoEditor);
	}
	
	private List<Image> findImages(File path) {
		File[] files = path.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				if(name.toLowerCase().endsWith(".jpg")) {
					return true;
				} else {
					return false;
				}
			}
		});
		if(files == null || files.length == 0) {
			return new ArrayList<>();
		}
		
		ArrayList<Image> images = new ArrayList<>();
		
		for(int i = 0; i < files.length; i++) {
			File file = files[i];
			Image image = new Image(file.toURI().toString());
			images.add(image);
		}
		return images;
	}
	
	private void scrollUp() {
		if(!scrollingBlocked) {
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					previousImage();
					block();
				}
			});			
		}
	}
	
	private void scrollDown() {
		if(!scrollingBlocked) {
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					nextImage();
					block();
				}
			});			
		}
	}
	
	//Animate
	private void swapBackground(Pane oldImagePane, Pane comingImagePane) {
		int duration = BLOCK_TIME;
		
		ScaleTransition st = new ScaleTransition(Duration.millis(duration), comingImagePane);
		st.setFromX(1.2);
		st.setFromY(1.2);
		st.setToX(1.0);
		st.setToY(1.0);

		FadeTransition ft = new FadeTransition(Duration.millis(duration), comingImagePane);
		ft.setFromValue(0.1);
		ft.setToValue(1.0);

		ParallelTransition p = new ParallelTransition(st, ft);

		appPane.getChildren().add(comingImagePane);

		p.play();
		
		p.setOnFinished(asdf -> {
			appPane.getChildren().remove(oldImagePane);
		});
		
		this.currentImagePane = comingImagePane;
		currentImagePane.setOnMouseClicked(e -> {
			pictureSelected();
		});
	}
	
	//Animate
	private void swapForeground(Pane oldImagePane, Pane comingImagePane) {
		int duration = BLOCK_TIME;
		
		appPane.getChildren().add(comingImagePane);
		comingImagePane.toBack();		
		
		ScaleTransition st = new ScaleTransition(Duration.millis(duration), oldImagePane);
		st.setFromX(1.0);
		st.setFromY(1.0);
		st.setToX(1.2);
		st.setToY(1.2);

		FadeTransition ft = new FadeTransition(Duration.millis(duration), oldImagePane);
		ft.setFromValue(1.0);
		ft.setToValue(0.1);

		ParallelTransition p = new ParallelTransition(st, ft);


		p.play();
		
		p.setOnFinished(asdf -> {
			appPane.getChildren().remove(oldImagePane);
		});
		
		this.currentImagePane = comingImagePane;
		currentImagePane.setOnMouseClicked(e -> {
			pictureSelected();
		});
	}
	
	
	public synchronized void block() {
		scrollingBlocked = true;
		/*Future<?> futureScrollingBlocked =*/ SchedulerUtils.scheduleRunOnce((int) (BLOCK_TIME * 1.5), new Runnable() {
			@Override
			public void run() {
				scrollingBlocked = false;
			}
		});
	}
	
	
	// Old snapshoter configuration values
	// Must be stored to restore them on quitStage
	private int oldPartial;
	private int oldFull;
	private int oldPeriod;
	private int[][] oldDitherMatrix;
	private OperationMode oldOperationMode;

	@Override
	public void enterStage(Context context) {
		this.context = context;
		
		StageSnapshoter snapshoter = context.getSnapshoter();
		oldPartial = snapshoter.getContinuousUpdateRefreshType();
		oldFull = snapshoter.getContinuousFullUpdateRefreshType();
		oldPeriod = snapshoter.getPeriod();
		oldOperationMode = snapshoter.getOperationMode();
		oldDitherMatrix = snapshoter.getDitherMatrix();
		
//		snapshoter.stop(); //update display only on demand 
//		snapshoter.setDitherMatrix(MonochromeFlexKitConnector.FlexModBayerDither);
//		snapshoter.setOperationMode(OperationMode.DITHER);
		
		SchedulerUtils.scheduleRunOnce(250, new Runnable() {
			@Override
			public void run() {
				snapshoter.manualFullDisplayUpdate();
			}
		});	
		
		
		block();
		
		discreteListener = new DeformationEventListener() {
			@Override
			public void newDeformationEvent(DeformationEvent deformationEvent) {
				switch (deformationEvent.getType()) {
				case UPPER_RIGHT_UP:
					pictureSelected();
					break;
				default:
					break;
				}
			}
		};
		context.getDeformationSensor().addDiscreteDeformationEventListener(discreteListener);
		
		continuousListener = new DeformationEventListener() {
			@Override
			public void newDeformationEvent(DeformationEvent deformationEvent) {
				switch (deformationEvent.getType()) {
				case MIDDLE_UP:
					scrollUp();
					break;
				case MIDDLE_DOWN:
					scrollDown();
					break;
				default:
					break;
				}
			}
		};
		context.getDeformationSensor().addContinuousDeformationEventListener(continuousListener);
	}
	
	@Override
	public void quitStage() {
		context.getDeformationSensor().removeContinuousDeformationEventListener(continuousListener);
		context.getDeformationSensor().removeDiscreteDeformationEventListener(discreteListener);
		
		StageSnapshoter snapshoter = context.getSnapshoter();
		snapshoter.setDitherMatrix(oldDitherMatrix);
		snapshoter.setOperationMode(oldOperationMode);
		snapshoter.setRefreshTypesForContinuousUpdates(oldPartial, oldFull);
		snapshoter.setPeriod(oldPeriod);
		snapshoter.requestFullUpdate();
//		snapshoter.start();
	}

	@Override
	public String getAppName() {
		return "Photo";
	}

	@Override
	public Pane getAppPane() {
		return appPane;
	}

	@Override
	public Image getAppImage() {
		return new Image(PhotoApp.class.getResourceAsStream("photo.png"));
	}
}
