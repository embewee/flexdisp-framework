package de.tu_darmstadt.informatik.tk.flexdisp.photoapp;

import java.io.IOException;
import java.text.DecimalFormat;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import de.tu_darmstadt.informatik.tk.flexdisp.deformationsensor.sensor.DeformationEvent;
import de.tu_darmstadt.informatik.tk.flexdisp.deformationsensor.sensor.DeformationEventListener;
import de.tu_darmstadt.informatik.tk.flexdisp.launchable.Context;
import de.tu_darmstadt.informatik.tk.flexdisp.launchable.Launchable;
import de.tu_darmstadt.informatik.tk.flexdisp.launchable.utils.FXUtils;
import de.tu_darmstadt.informatik.tk.flexdisp.launchable.utils.SchedulerUtils;
import de.tu_darmstadt.informatik.tk.flexkit.jconnector.MonochromeFlexKitConnector;
import de.tu_darmstadt.informatik.tk.flexkit.jconnector.OperationMode;
import de.tu_darmstadt.informatik.tk.flexkit.jconnector.StageSnapshoter;

/**
 * Photo editor view of photo applet.
 * @author Michael Winkler
 *
 */
public class PhotoEditor implements Launchable {
	
	private enum PhotoEffect {
		HUE,
		SCALE,
		ROTATE,
	}
	
	private Context context;
	private VBox root;
	private ImageView editImageView;
	private AnchorPane editImageViewContainer;
	private ToggleGroup effectToggleGroup;
	private DeformationEventListener continuousListener;
	private ColorAdjust colorAdjust;
	private boolean scrollingBlocked = false;

	/**
	 * @param image The Image to modify
	 */
	public PhotoEditor(Image image) {
		root = new VBox();
		root.setOnScroll(event -> {
			int sign = (int) Math.signum(event.getDeltaY());
			if (sign > 0) {
				effectUp();
			} else if (sign < 0) {
				effectDown();
			}
		});
		
		colorAdjust = new ColorAdjust();
		
		Image backgroundImage = new Image(getClass().getResourceAsStream("bg-empty.jpg"));
		root.setBackground(new Background(new BackgroundImage(backgroundImage, 
				BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, BackgroundSize.DEFAULT)));

		//make top element and add "Effects" label
		HBox top = new HBox();
		top.setPadding(new Insets(10, 10, 10, 10));
		Region spacer = new Region();
		HBox.setHgrow(spacer, Priority.ALWAYS);
		Label lblEffect = new Label("Effects");
		lblEffect.setStyle("-fx-font-size: 40px; -fx-font-weight: bold;");
		lblEffect.setTextFill(Color.WHITE);
		lblEffect.minWidth(100);
		lblEffect.prefWidth(100);
		lblEffect.maxWidth(100);
		top.getChildren().addAll(spacer, lblEffect);
		root.getChildren().add(top);
		
		HBox editorHBox = null;
		VBox effectsView = null;
		
		try {
			editorHBox = FXMLLoader.load(getClass().getResource("PhotoEditor.fxml"));
			editImageView = FXUtils.getChildByID(editorHBox, "editImageView");
			effectsView = FXUtils.getChildByID(editorHBox, "effectsView");
			editImageViewContainer = FXUtils.getChildByID(editorHBox, "editImageViewContainer");
		} catch (IOException e) {
			e.printStackTrace(); //FIXME
		}
		
		editImageView.setImage(image);
		effectsView = makeEffectButtons(effectsView);
		root.getChildren().add(editorHBox);
		
		AnchorPane.setTopAnchor(editorHBox, 10.0);
		AnchorPane.setRightAnchor(editorHBox, 10.0);
		AnchorPane.setBottomAnchor(editorHBox, 10.0);
		AnchorPane.setLeftAnchor(editorHBox, 10.0);
	}	
	
	/**
	 * Creates the effect buttons and places them in a VBox
	 * @param box The VBox to place the buttons in
	 * @return The VBox with buttons
	 */
	private VBox makeEffectButtons(VBox box) {
		effectToggleGroup = new ToggleGroup();
		ToggleButton toggleButton;

		toggleButton = makeToggleButton("Farbton", PhotoEffect.HUE, effectToggleGroup);
		toggleButton.setSelected(true);
		final Label lblHue = makeInfoLabel("0%");
		toggleButton.selectedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				lblHue.setVisible(newValue);
			}
		});
		colorAdjust.hueProperty().addListener(new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				Double d = (Double) newValue;
				DecimalFormat df2 = new DecimalFormat("#%");
				lblHue.setText(df2.format(d));
			}
		});
		HBox hue = new HBox();
		hue.getChildren().addAll(lblHue, toggleButton);
		
		toggleButton = makeToggleButton("Skalieren", PhotoEffect.SCALE, effectToggleGroup);
		final Label lblScale = makeInfoLabel("100%");
		toggleButton.selectedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				lblScale.setVisible(newValue);
			}
		});
		editImageViewContainer.scaleXProperty().addListener(new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				Double d = (Double) newValue;
				DecimalFormat df2 = new DecimalFormat("#%");
				lblScale.setText(df2.format(d));				
			}
		});
		HBox scale = new HBox();
		scale.getChildren().addAll(lblScale, toggleButton);
		
		toggleButton = makeToggleButton("Rotieren", PhotoEffect.ROTATE, effectToggleGroup);
		final Label lblRotate= makeInfoLabel("0�");
		toggleButton.selectedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				lblRotate.setVisible(newValue);
			}
		});
		editImageViewContainer.rotateProperty().addListener(new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				Double d = (Double) newValue;
				d = (d % 360 + 360) % 360;
				DecimalFormat df2 = new DecimalFormat("#");
				lblRotate.setText(df2.format(d) + "�");				
			}
		});
		HBox rotate = new HBox();
		rotate.getChildren().addAll(lblRotate, toggleButton);
		
		box.getChildren().addAll(/*contrast,*/ hue, /*brightness, saturation,*/ scale, rotate);
		return box;
	}
	
	/**
	 * Create a styled ToggleButton
	 * @param title
	 * @param photoEffect
	 * @param group
	 * @return
	 */
	private ToggleButton makeToggleButton(String title, PhotoEffect photoEffect, ToggleGroup group) {
		ToggleButton toggleButton = new ToggleButton(title);
		toggleButton.setUserData(photoEffect);		
		toggleButton.setToggleGroup(group);
		toggleButton.setMinWidth(200);
		toggleButton.setMinHeight(80);
		return toggleButton;
	}
	
	/**
	 * Create a styled Label
	 * @param text
	 * @return
	 */
	private Label makeInfoLabel(String text) {
		Label lbl = new Label(text);
		DropShadow shadow = new DropShadow(2.0, Color.BLACK);
		lbl.setEffect(shadow);
		lbl.setVisible(false);
		lbl.setMinHeight(80);
		lbl.setMinWidth(80);
		lbl.setStyle("-fx-font-size: 20px; -fx-font-weight: bold;");
		lbl.setTextFill(Color.WHITE);
		return lbl;
	}
	
	// Old snapshoter configuration values
	// Must be stored to restore them on quitStage
	private int oldPartial;
	private int oldFull;
	private int oldPeriod;
	private int[][] oldDitherMatrix;
	private OperationMode oldOperationMode;
	
	@Override
	public void enterStage(Context context) {
		this.context = context;
		context.addCSS(getClass().getResource("Buttons.css").toExternalForm());
		
		StageSnapshoter snapshoter = context.getSnapshoter();
		oldPartial = snapshoter.getContinuousUpdateRefreshType();
		oldFull = snapshoter.getContinuousFullUpdateRefreshType();
		oldPeriod = snapshoter.getPeriod();
		oldOperationMode = snapshoter.getOperationMode();
		oldDitherMatrix = snapshoter.getDitherMatrix();
		
		snapshoter.stop(); //update display only on demand 
		snapshoter.setDitherMatrix(MonochromeFlexKitConnector.FlexModBayerDither);
		snapshoter.setOperationMode(OperationMode.DITHER);
		
		SchedulerUtils.scheduleRunOnce(250, new Runnable() {
			@Override
			public void run() {
				snapshoter.manualFullDisplayUpdate();
			}
		});	
		
		continuousListener = new DeformationEventListener() {
			@Override
			public void newDeformationEvent(DeformationEvent deformationEvent) {
				switch (deformationEvent.getType()) {
				case MIDDLE_UP:
					bendingUp();
					break;
					
				case RESTING:
					resting();
					break;
					
				case MIDDLE_DOWN:
					bendingDown();
					break;

				default:
					break;
				}
			}
		};
		context.getDeformationSensor().addContinuousDeformationEventListener(continuousListener);
	}
	
	@Override
	public void quitStage() {
		context.removeCSS(getClass().getResource("Buttons.css").toExternalForm());
		context.getDeformationSensor().removeContinuousDeformationEventListener(continuousListener);
		
		StageSnapshoter snapshoter = context.getSnapshoter();
		snapshoter.setDitherMatrix(oldDitherMatrix);
		snapshoter.setOperationMode(oldOperationMode);
		snapshoter.setRefreshTypesForContinuousUpdates(oldPartial, oldFull);
		snapshoter.setPeriod(oldPeriod);
		snapshoter.requestFullUpdate();
		snapshoter.start();
	}
	
	@Override
	public String getAppName() {
		return "Photo";
	}

	@Override
	public Pane getAppPane() {
		return root;
	}

	@Override
	public Image getAppImage() {
		return new Image(PhotoApp.class.getResourceAsStream("photo.png"));
	}

double acceleration = 1.0;
	
	private void bendingUp() {
		if(!scrollingBlocked){
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					effectUp();
					acceleration = acceleration * 1.1;
				}
			});
			block();
		}
	}
	
	private void resting() {
		acceleration = 1.0;
	}
	
	private void bendingDown() {
		if(!scrollingBlocked) {
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					effectDown();
					acceleration = acceleration * 1.1;
				}
			});
			block();
		}
	}
	
	private void effectUp() {
		Toggle toggle = effectToggleGroup.getSelectedToggle();
		if (toggle == null) 
			return;
		
		PhotoEffect effectType = (PhotoEffect) toggle.getUserData();
		double nextValue;
		
		switch(effectType) {
		case HUE:
			nextValue = Math.min(colorAdjust.getHue() + 0.05, 1.0);
			colorAdjust.setHue(nextValue);
			editImageView.setEffect(colorAdjust);
			break;
			
		case SCALE:
			editImageViewContainer.setScaleX(editImageViewContainer.getScaleX() + 0.1 * acceleration);
			editImageViewContainer.setScaleY(editImageViewContainer.getScaleY() + 0.1 * acceleration);
			break;
			
		case ROTATE:
			editImageViewContainer.setRotate(editImageViewContainer.getRotate() + 4.0 * acceleration);
			
		default:
			break;
		}
		context.getSnapshoter().manualPartialDisplayUpdate();
	}
	
	private void effectDown() {
		Toggle toggle = effectToggleGroup.getSelectedToggle();
		if (toggle == null) 
			return;
		
		PhotoEffect effect = (PhotoEffect) toggle.getUserData();
		double nextValue;
		
		switch(effect) {
		case HUE:
			nextValue = Math.max(colorAdjust.getHue() - 0.05, -1.0);
			colorAdjust.setHue(nextValue);
			editImageView.setEffect(colorAdjust);
			break;
			
		case SCALE:
			editImageViewContainer.setScaleX(Math.max(0, editImageViewContainer.getScaleX() - 0.1 * acceleration));
			editImageViewContainer.setScaleY(Math.max(0, editImageViewContainer.getScaleY() - 0.1 * acceleration));
			break;
			
		case ROTATE:
			editImageViewContainer.setRotate(editImageViewContainer.getRotate() - 4.0 * acceleration);
			break;
			
		default:
			break;
		}
		context.getSnapshoter().manualPartialDisplayUpdate();
	}
	
	public synchronized void block() {
		scrollingBlocked = true;
		SchedulerUtils.scheduleRunOnce(300, new Runnable() {
			@Override
			public void run() {
				scrollingBlocked = false;
			}
		});
	}
}
