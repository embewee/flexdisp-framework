package de.tu_darmstadt.informatik.tk.flexdisp.deformationsensor.spy;


/**
 * Starts a new deformation spy. 
 * @author Michael Winkler
 *
 */
public class SpyMain {
	
	public static void main(String[] args) {
		String port = "COM3";
		if(args.length == 1) {
			port = args[0];
		}
		new Spy(port);
	}

}
