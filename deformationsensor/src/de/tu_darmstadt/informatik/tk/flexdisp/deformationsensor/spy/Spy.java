package de.tu_darmstadt.informatik.tk.flexdisp.deformationsensor.spy;

import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import de.tu_darmstadt.informatik.tk.flexdisp.deformationsensor.sensor.ArduinoConnector;
import de.tu_darmstadt.informatik.tk.flexdisp.deformationsensor.sensor.ArduinoException;
import de.tu_darmstadt.informatik.tk.flexdisp.deformationsensor.sensor.DeformationInputDispatcher;

/**
 * Some code to continuously take samples from the deformation sensor. 
 * @author Michael Winkler
 *
 */
public class Spy extends DeformationInputDispatcher{
	
	private enum MODE {
		NONE,
		MENU_A,
		MENU_B,
		MENU_C,
		MENU_D,
		MENU_E,
		DEFTOUCH4,
		DEFTOUCH2,
		KARTE,
		MUSIK,
		SAVED;
	}
	
	private MODE currentMode;
	
	private JFrame spyWindow;
	private ArduinoConnector connector;
	private Thread thread;
	private File outputFile;
	private FileWriter fw;
	
	private JLabel lbl1;
	private JLabel lbl2;
	private JLabel lbl3;
	private JLabel lbl4;
	private JLabel lblMode;
	private JLabel lblFile;
	
	public Spy(String serialPort) {
		
		currentMode = MODE.NONE;
		
		spyWindow = new JFrame("Deformation Spy");
		spyWindow.addWindowListener(new WindowAdapter() {
		    @Override
		    public void windowClosed(WindowEvent e) {
		    	try {
					close();
				} catch (ArduinoException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} finally {
					System.exit(0);
				}
		    }
		});
		
		spyWindow.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(currentMode == MODE.SAVED) {
					return;
				}
				
				switch (e.getKeyChar()) {
				case 'a':
				case 'A':
					mode(MODE.MENU_A);
					break;
				case 'b':
				case 'B':
					mode(MODE.MENU_B);
					break;
				case 'c':
				case 'C':
					mode(MODE.MENU_C);
					break;
				case 'd':
				case 'D':
					mode(MODE.MENU_D);
					break;
				case 'e':
				case 'E':
					mode(MODE.MENU_E);
					break;
				case '2':
					mode(MODE.DEFTOUCH2);
					break;
				case '4':
					mode(MODE.DEFTOUCH4);
					break;
				case 'k':
				case 'K':
					mode(MODE.KARTE);
					break;
				case 'm':
				case 'M':
					mode(MODE.MUSIK);
					break;
					
				case 'S':
					save();
					mode(MODE.SAVED);
					break;

				default:
					break;
				}
			}
			
		});

		spyWindow.setSize(600, 250);
		
		lbl1 = new JLabel("X");
		lbl2 = new JLabel("X");
		lbl3 = new JLabel("X");
		lbl4 = new JLabel("X");
		lblMode = new JLabel("...");
		lblFile = new JLabel("...");
		
		outputFile = new File("C:\\Temp\\spy\\spy_" + time() + ".txt");
		lblFile.setText(outputFile.getAbsolutePath());
		
		try {
			fw = new FileWriter(outputFile, true);//the true will append the new data
		} catch (IOException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		} 
		
		Container pane = spyWindow.getContentPane();
		pane.setLayout(new GridLayout(0, 2));
		
		pane.add(new JLabel("Stripe 1:"));
		pane.add(lbl1);
		pane.add(new JLabel("Stripe 2:"));
		pane.add(lbl2);
		pane.add(new JLabel("Stripe 3:"));
		pane.add(lbl3);
		pane.add(new JLabel("Stripe 4:"));
		pane.add(lbl4);
		pane.add(new JLabel());
		pane.add(new JLabel());
		pane.add(new JLabel("Mode:"));
		pane.add(lblMode);
		pane.add(new JLabel("File:"));
		pane.add(lblFile);
		
		spyWindow.setVisible(true);
		
		connector = new ArduinoConnector(serialPort, 17);
		try {
			connector.connect();
		} catch (ArduinoException e1) {
			JOptionPane.showMessageDialog(spyWindow, e1.getMessage(), "Error!", JOptionPane.ERROR_MESSAGE);
			e1.printStackTrace();
		}
		thread = new Thread(connector);
		thread.start();
		
		connector.addDispather(this);
		
	}
	
	private void mode(MODE m) {
		lblMode.setText(m.toString());
		write(m.toString()+"\n");
		currentMode = m;
	}
	
	private String time() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MMM-dd__HH-mm-ss");
		Date d = new Date();
		return sdf.format(d);
	}
	
	private String timeShort() {
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss;");
		Date d = new Date();
		return sdf.format(d);
	}
	
	private void close() throws ArduinoException, IOException {
		fw.close();
		connector.disconnect();
		System.out.println("Closed");
	}

	@Override
	public void dispatch(double val1, double val2, double val3, double val4) {
		String v1 = Double.toString(val1);
		String v2 = Double.toString(val2);
		String v3 = Double.toString(val3);
		String v4 = Double.toString(val4);
		
		lbl1.setText(v1);
		lbl2.setText(v2);
		lbl3.setText(v3);
		lbl4.setText(v4);
		
		switch(currentMode) {
		case NONE:
			return;
		default:
			StringBuffer sb = new StringBuffer();
			sb.append(timeShort());
			sb.append(v1).append(";");
			sb.append(v2).append(";");
			sb.append(v3).append(";");
			sb.append(v4).append(";\n");

			write(sb.toString());
			break;
		}
		
	}
	
	private void save() {
		try {
			fw.flush();
			fw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.exit(0);
	}
	
	private synchronized void write(String s) {
		try	{
		    fw.write(s);
		} catch(IOException ioe) {
		    System.err.println("IOException: " + ioe.getMessage());
		}
	}
}
