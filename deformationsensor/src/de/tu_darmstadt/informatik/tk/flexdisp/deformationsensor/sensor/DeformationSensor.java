package de.tu_darmstadt.informatik.tk.flexdisp.deformationsensor.sensor;

import java.io.File;
import java.io.IOException;

/**
 * Connects to the arduino, to get values from the deformation sensor.
 * Uses a kNN-classifier, to classify these values into deformation gestures.
 * @author Michael Winkler
 *
 */
public class DeformationSensor {
	private ArduinoConnector connector;
	private Thread thread;
	
	private DeformationKnnClassifier classifier;
	private File classifierLearnedDataSet;
	private int classifierKValue;
	private int classifierSmoothing;
	
	/**
	 * 
	 * @param serialPort
	 * @param dry
	 * @param learnedDataSet
	 * @param kValue
	 * @param smoothing
	 * @throws ArduinoException
	 * @throws IOException
	 */
	public DeformationSensor(String serialPort, boolean dry, 
			File learnedDataSet, int kValue, int smoothing) throws ArduinoException {
		classifierLearnedDataSet = learnedDataSet;
		classifierKValue = kValue;
		classifierSmoothing = smoothing;
		
		connector = new ArduinoConnector(serialPort, 17);
		connector.dry(dry);
		connector.connect();
		thread = new Thread(connector);
		thread.start();
	}
	
	private void createClassifier() {
		if(classifier == null) {
			try {
				classifier = new DeformationKnnClassifier(
						classifierLearnedDataSet, classifierKValue, classifierSmoothing);
			} catch (IOException e) {
				e.printStackTrace(); //FIXME
			}
			connector.addDispather(classifier);
		}
	}
	
	public void addRawValueDispatcher(DeformationInputDispatcher dispatcher) {
		connector.addDispather(dispatcher);
	}
	
	public void removeRawValueDispatcher(DeformationInputDispatcher dispatcher) {
		connector.removeDispatcher(dispatcher);
	}
	
	public void addDiscreteDeformationEventListener(DeformationEventListener e) {
		createClassifier();		
		classifier.addDiscreteDeformationEventListener(e);
	}

	public void addContinuousDeformationEventListener(DeformationEventListener e) {
		createClassifier();
		classifier.addContinuousDeformationEventListener(e);
	}

	public void removeDiscreteDeformationEventListener(DeformationEventListener e) {
		createClassifier();
		classifier.removeDiscreteDeformationEventListener(e);
	}
	
	public void removeContinuousDeformationEventListener(DeformationEventListener e) {
		createClassifier();
		classifier.removeContinuousDeformationEventListener(e);
	}
	
	/**
	 * Stops the deformation sensor. It cannot be restarted. FIXME
	 * @throws ArduinoException
	 */
	public void stop() throws ArduinoException {
		connector.disconnect();
	}
}
