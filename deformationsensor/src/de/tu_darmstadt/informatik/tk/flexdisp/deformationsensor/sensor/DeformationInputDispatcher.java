package de.tu_darmstadt.informatik.tk.flexdisp.deformationsensor.sensor;

/**
 * This class resolves the byte array received from the arduino into 
 * one raw bend value for each bend sensor. The deformation sensor is built 
 * of four bend sensors. <p>
 * Register this class at the ArduinoConnector.<p>
 * The abstract dispatch()-method needs to be implemented. 
 * @author Michael Winkler
 */
public abstract class DeformationInputDispatcher implements RawInputDispatcher{

	//The arduino delivers a 17 byte array containing the actual values and delimiters.
	//This bytes of the array belong to a sensor value of a bend sensor stripe. 
	private Integer[] wantedBytes1 = new Integer[]{1,2,3};
	private Integer[] wantedBytes2 = new Integer[]{5,6,7};
	private Integer[] wantedBytes3 = new Integer[]{9,10,11};
	private Integer[] wantedBytes4 = new Integer[]{13,14,15};
	
	@Override
	public void dispatchRawInput(byte[] inputArray) {
		//Werte in array sind ASCII, d.h. Ziffer 0 hat ASCII-Wert 48
		
		int val1 = 0;
		for(int i = 0; i < wantedBytes1.length; i++) {
			int multiplicator = (int) Math.pow(10, (wantedBytes1.length - 1 - i));
			val1 = val1 + ((inputArray[wantedBytes1[i]] - 48) * multiplicator);
		}
		
		int val2 = 0;
		for(int i = 0; i < wantedBytes2.length; i++) {
			int multiplicator = (int) Math.pow(10, (wantedBytes1.length - 1 - i));
			val2 = val2 + ((inputArray[wantedBytes2[i]] - 48) * multiplicator);
		}
		
		int val3 = 0;
		for(int i = 0; i < wantedBytes3.length; i++) {
			int multiplicator = (int) Math.pow(10, (wantedBytes1.length - 1 - i));
			val3 = val3 + ((inputArray[wantedBytes3[i]] - 48) * multiplicator);
		}
		
		int val4 = 0;
		for(int i = 0; i < wantedBytes4.length; i++) {
			int multiplicator = (int) Math.pow(10, (wantedBytes1.length - 1 - i));
			val4 = val4 + ((inputArray[wantedBytes4[i]] - 48) * multiplicator);
		}
		
		dispatch(val1, val2, val3, val4);
	}
	
	/**
	 * Receives the raw input values from the bend sensors.
	 * @param val1 double Raw input value from bend sensor 1. 
	 * @param val2 double Raw input value from bend sensor 2. 
	 * @param val3 double Raw input value from bend sensor 3. 
	 * @param val4 double Raw input value from bend sensor 4. 
	 */
	public abstract void dispatch(double val1, double val2, double val3, double val4);

}
