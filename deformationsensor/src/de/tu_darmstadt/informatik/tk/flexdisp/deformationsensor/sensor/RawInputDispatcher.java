package de.tu_darmstadt.informatik.tk.flexdisp.deformationsensor.sensor;

/**
 * Interface that declares the <code>dispatch()</code> method. The <code>ArduinoRXTX</code> class consumes
 * a Dispatcher.
 */
public interface RawInputDispatcher {
	
	/**
	 * Dispatches an byte array received from the Arduino.
	 * @param arr byte[] The byte array received from Arduino. 
	 */
	public void dispatchRawInput(byte[] arr);

}
