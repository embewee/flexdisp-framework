package de.tu_darmstadt.informatik.tk.flexdisp.deformationsensor.sensor;

import gnu.io.CommPortIdentifier;
import gnu.io.NoSuchPortException;
import gnu.io.PortInUseException;
import gnu.io.RXTXPort;
import gnu.io.SerialPort;
import gnu.io.UnsupportedCommOperationException;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Set;

/**
 *  Class for interaction with an Arduino board on hardware level.<br>
 *  Makes use of the gnu.io package to communicate with an Arduino board over a serial connection.
 *  @author Michael Winkler
 */
public class ArduinoConnector implements Runnable {
	/** Length of the array to be read from the Arduino. */
	private int inputArrayLength;
	/** Speed of connection to the Arduino. */
	public final static int BAUD_RATE = 9600;
	public final static String PORT_OWNER = "ARDUINO_RXTX";
	
	private boolean dryRun;
	private boolean connected;
	private boolean run;
	
	private String portName;
	private Set<RawInputDispatcher> dispatchers;
	private CommPortIdentifier portId;
	private RXTXPort serialPort;
	private InputStream inputStream;

	/**
	 * IMPORTANT: Call <code>connect()</code> before <code>start()</code>!
	 * @param portName String The port to connect to (eg. "COM3" or "/dev/ttyUSB0")
	 * @param dispatcher An concrete implementation of the <code>Dispatcher</code> interface
	 * which dispatches the array read from Arduino.
	 */
	public ArduinoConnector(String portName, int inputArrayLength) {
		this.portName = portName;
		this.inputArrayLength = inputArrayLength;
		
		dispatchers = new HashSet<>();
		
		dryRun = false;
		connected = false;
		portId = null;
		serialPort = null;
		inputStream = null;
	}
	
	/**
	 * If dry is set to <code>true</code>, no real connection is established, but the ArduinoConnector 
	 * simulates a connection. In this case, no data from the Arduino is received.
	 * @param dry
	 */
	public void dry(boolean dry) {
		this.dryRun = dry;
	}
	
	
	/**
	 * Establishes connection to the Arduino board.
	 * @throws ArduinoException
	 */
	public synchronized void connect() throws ArduinoException {
		if (connected) {
			return;
		}
		
		run = true;
		
		if(dryRun) {
			return;
		}
		
		try {
			portId = CommPortIdentifier.getPortIdentifier(portName);
			serialPort = (RXTXPort) portId.open(PORT_OWNER, 500);
			serialPort.notifyOnDataAvailable(true);
			serialPort.setSerialPortParams(
					BAUD_RATE, 
					SerialPort.DATABITS_8,
					SerialPort.STOPBITS_1, 
					SerialPort.PARITY_NONE);
			serialPort.setFlowControlMode(SerialPort.FLOWCONTROL_NONE);
			inputStream = serialPort.getInputStream();
			connected = true;
		} catch (UnsupportedCommOperationException | NoSuchPortException | PortInUseException e) {
			throw new ArduinoException(e, "Connect to Arduino failed (port: " + portName + ").");
		}
	}
	
	/**
	 * Adds the given dispatcher to the set of dispatchers, which
	 * is informed about new raw data from the deformation sensor.
	 * @param dispatcher RawInputDispatcher
	 */
	public void addDispather(RawInputDispatcher dispatcher) {
		dispatchers.add(dispatcher);
	}
	
	/**
	 * Removes the given dispatcher from the set of dispatchers, which
	 * is informed about new raw data from the deformation sensor.
	 * @param dispatcher RawInputDispatcher
	 */
	public void removeDispatcher(RawInputDispatcher dispatcher) {
		dispatchers.remove(dispatcher);
	}

	/**
	 * Tear down the connection to the Arduino board.
	 * @throws IOException
	 */
	public synchronized void disconnect() throws ArduinoException {
		if (connected) {
			if(dryRun) {
				run = false;
				connected = false;
			} else {
				try {
					run = false;
					inputStream.close();
					inputStream = null;
					serialPort.close();
					serialPort = null;
					portId = null;
					connected = false;
				} catch (IOException e) {
					throw new ArduinoException(e, "IOException: Could not disconnect.");
				}
			}
		}
	}

	@Override
	public void run(){
		if(!isConnected() && !dryRun) {
			System.err.println("WARNING! No connection to the arduino device "
					+ "has been established. Call 'connect()' before 'start()'.");
		}
		
		if(dryRun) {
			while(run) {
				//nop
			}
			
		} else {
			while(run) {
				try {
					int availableBytes = inputStream.available();
					
					if (availableBytes == inputArrayLength) {
						byte[] bb = new byte[inputArrayLength];
						inputStream.read(bb);
						informDispatchers(bb);
					} else if (availableBytes > inputArrayLength) {
						inputStream.skip(availableBytes);
					}
				} catch (IOException e) {
					// TODO
					System.err.println("Could not read from input stream.");
					e.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * Informs all registered dispatchers
	 * @param bb Raw input array
	 */
	private void informDispatchers(byte[] bb) {
		for (RawInputDispatcher dispatcher : dispatchers) {
			dispatcher.dispatchRawInput(bb);
		}
	}

	public boolean isConnected() {
		return connected;
	}
}
