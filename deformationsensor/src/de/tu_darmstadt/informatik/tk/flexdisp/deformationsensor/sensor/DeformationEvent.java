package de.tu_darmstadt.informatik.tk.flexdisp.deformationsensor.sensor;

/**
 * This class contains an enumeration of deformation event types. Instances of this class
 * are passed to listeners to inform about new deformation events. 
 * @author Michael Winkler
 */
public class DeformationEvent {
	
	/** Defined BendEvent types */
	public enum Type {
		RESTING,
		MIDDLE_UP,
		MIDDLE_DOWN,
		UPPER_RIGHT_UP,
		UPPER_RIGHT_DOWN,
		BOTTOM_RIGHT_UP,
		BOTTOM_RIGHT_DOWN,
		UPPER_LEFT_UP,
		UPPER_LEFT_DOWN,
		TWIST_RIGHT_UP,
		TWIST_RIGHT_DOWN,
		EDGE_RIGHT_UP,
		EDGE_RIGHT_DOWN,
		EDGE_LEFT_UP,
		EDGE_LEFT_DOWN,
	}
	
	/**
	 * The concrete type of deformation event of the actual instance 
	 * of DeformationEvent.
	 */
	private Type eventType;
	
	/**
	 * Creates a new deformation event. 
	 * @param eventType Enum constant of specific deformation event
	 */
	public DeformationEvent(Type eventType) {
		this.eventType = eventType;
	}
	
	/**
	 * @return DeformationEvent.Type: deformation event's enum value 
	 */
	public Type getType() {
		return eventType;
	}
	
	/**
	 * @return DeformationEvent.Type: deformation event's enum value String representation
	 */
	public String getName() {
		return eventType.toString();
	}

}
