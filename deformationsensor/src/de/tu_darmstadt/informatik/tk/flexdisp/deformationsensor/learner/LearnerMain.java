package de.tu_darmstadt.informatik.tk.flexdisp.deformationsensor.learner;

/**
 * Starts a small utility program, which can be used to collect 
 * deformation gestures. The serial port to connect to can be specified
 * by just using the port's name as command line argument. Otherwise default
 * port "COM3" is used.
 */
public class LearnerMain {
	public static void main(String[] args) {
		String port = "COM3";
		if(args.length == 1) {
			port = args[0];
		}
		new LearnerWindow(port);
	}
}
