package de.tu_darmstadt.informatik.tk.flexdisp.deformationsensor.learner;

import java.awt.Container;
import java.awt.GridLayout;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import de.tu_darmstadt.informatik.tk.flexdisp.deformationsensor.sensor.ArduinoConnector;
import de.tu_darmstadt.informatik.tk.flexdisp.deformationsensor.sensor.ArduinoException;
import de.tu_darmstadt.informatik.tk.flexdisp.deformationsensor.sensor.DeformationEvent;
import de.tu_darmstadt.informatik.tk.flexdisp.deformationsensor.sensor.DeformationEventListener;
import de.tu_darmstadt.informatik.tk.flexdisp.deformationsensor.sensor.DeformationInputDispatcher;
import de.tu_darmstadt.informatik.tk.flexdisp.deformationsensor.sensor.DeformationKnnClassifier;
import de.tu_darmstadt.informatik.tk.flexdisp.deformationsensor.sensor.SchedulerUtils;

/**
 * Simple Swing window to collect training data and evaluate collected training data. 
 * @author Michael Winkler
 */
public class LearnerWindow extends DeformationInputDispatcher {
	private JFrame learnerWindow;
	
	/**
	 * Delay before taking samples after clicking on 'Take Samples'
	 */
	private int INITIAL_DELAY = 2000;
	
	private String title = "Deformation Sensor";

	private JLabel lbl1;
	private JLabel lbl2;
	private JLabel lbl3;
	private JLabel lbl4;
	private JLabel lblPredictedClass;
	private JLabel lblProgress;
	private JComboBox<DeformationEvent.Type> comboClasses;
	private JTextField txtNrOfSamples;
	private JTextField txtFile;
	private JButton btnSample;
	private JButton btnSave;
	private double val1;
	private double val2;
	private double val3;
	private double val4;
	private JTextField txtK;
	private JTextField txtSmoothing;
	
	private int currentLearningClass = 0;
	
	private DeformationKnnClassifier classifier;
	
	enum Mode {
		LEARN,
		CLASSIFY;
	}
	private Mode mode = Mode.LEARN;
	
	private ArduinoConnector connector;
	private Thread thread;
	
	private ArrayList<double[]> learning; 
	
	public LearnerWindow(String serialPort) {
		learnerWindow = new JFrame(title + " (LEARNING MODE)");
		learnerWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		learnerWindow.setSize(600, 500);
		
		learning = new ArrayList<>();
		
		Container pane = learnerWindow.getContentPane();
		pane.setLayout(new GridLayout(0, 2));
		
		JButton btnModeLearn = new JButton("Mode: Learn");
		btnModeLearn.addActionListener(e -> {
			mode = Mode.LEARN;
			learnerWindow.setTitle(title + " (LEARNING MODE)");
			btnSave.setEnabled(true);
			btnSample.setEnabled(true);
			lblPredictedClass.setText("---");
			txtK.setEnabled(true);
			txtSmoothing.setEnabled(true);
			modeLearn();
		});
		
		JButton btnModeClassify = new JButton("Mode: Classify");
		btnModeClassify.addActionListener(e -> {
			if(modeClassify()) {
				mode = Mode.CLASSIFY;
				learnerWindow.setTitle(title + " (CLASSIFICATION MODE)");
				btnSave.setEnabled(false);
				btnSample.setEnabled(false);
				txtK.setEnabled(false);
				txtSmoothing.setEnabled(false);
			}
		});
		
		
		lbl1 = new JLabel("X");
		lbl2 = new JLabel("X");
		lbl3 = new JLabel("X");
		lbl4 = new JLabel("X");
		
		txtNrOfSamples = new JTextField("25");
		
		comboClasses = new JComboBox<>(DeformationEvent.Type.values());
		comboClasses.setSelectedIndex(0);
		comboClasses.addActionListener(e -> {
			JComboBox<?> cb = (JComboBox<?>)e.getSource();
//			DeformationEvent.Type type = (DeformationEvent.Type) cb.getSelectedItem();
			currentLearningClass = cb.getSelectedIndex();
	        System.out.println(currentLearningClass);
		});
		
		
		
		
		
		btnSample = new JButton("Take Samples");
		btnSample.addActionListener(e -> {
			takeSamples();
		});
		
		lblProgress = new JLabel("Take samples!");
				
		txtFile = new JTextField("C:\\Temp\\deformationsensor\\dataset" + System.currentTimeMillis() + ".txt");
		
		btnSave = new JButton("Save");
		btnSave.addActionListener(e -> {
			File file = new File(txtFile.getText());
			try {
				saveLearnedData(file);
			} catch (Exception e1) {
				JOptionPane.showMessageDialog(learnerWindow, e1.getMessage(), "Error!", JOptionPane.ERROR_MESSAGE);
				e1.printStackTrace();
			}
		});
		
		txtK = new JTextField("1");
		txtSmoothing = new JTextField("3");
		
		
		lblPredictedClass = new JLabel();
		
		pane.add(btnModeLearn);
		pane.add(btnModeClassify);
		pane.add(new JLabel("Stripe 1:"));
		pane.add(lbl1);
		pane.add(new JLabel("Stripe 2:"));
		pane.add(lbl2);
		pane.add(new JLabel("Stripe 3:"));
		pane.add(lbl3);
		pane.add(new JLabel("Stripe 4:"));
		pane.add(lbl4);
		pane.add(new JLabel("Nr. of samples:"));
		pane.add(txtNrOfSamples);
		pane.add(new JLabel("Class:"));
		pane.add(comboClasses);
		pane.add(new JLabel());
		pane.add(btnSample);
		pane.add(new JLabel());
		pane.add(new JLabel("Initial delay: " + (INITIAL_DELAY / 1000) + " sec."));
		pane.add(new JLabel());
		pane.add(lblProgress);
		pane.add(new JLabel());
		pane.add(new JLabel());
		pane.add(new JLabel("File (save learned/open for classification):"));
		pane.add(txtFile);
		pane.add(new JLabel());
		pane.add(btnSave);
		pane.add(new JLabel());
		pane.add(new JLabel());
		pane.add(new JLabel("Classifier k-value"));
		pane.add(txtK);
		pane.add(new JLabel("Classifier smoothing"));
		pane.add(txtSmoothing);
		pane.add(new JLabel());
		pane.add(new JLabel());
		pane.add(new JLabel("Predicted Class:"));
		pane.add(lblPredictedClass);
		
		learnerWindow.setVisible(true);
		
		connector = new ArduinoConnector(serialPort, 17);
		try {
			connector.connect();
		} catch (ArduinoException e1) {
			JOptionPane.showMessageDialog(learnerWindow, e1.getMessage(), "Error!", JOptionPane.ERROR_MESSAGE);
			e1.printStackTrace();
		}
		thread = new Thread(connector);
		thread.start();
		
		connector.addDispather(this);
	}
	
	private void modeLearn() {
		classifier = null;
	}
	
	private boolean modeClassify() {
		File learnedDataSet = new File(txtFile.getText());
		try {
			int kValue = Integer.parseInt(txtK.getText());
			int smoothing = Integer.parseInt(txtSmoothing.getText());
			
			classifier = new DeformationKnnClassifier(learnedDataSet, kValue, smoothing);
		} catch (IOException e) {
			JOptionPane.showMessageDialog(learnerWindow, e.getMessage(), "Error!", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
		classifier.addContinuousDeformationEventListener(new DeformationEventListener() {
			@Override
			public void newDeformationEvent(DeformationEvent deformationEvent) {
				lblPredictedClass.setText(deformationEvent.getName());
			}
		});
		return true;
	}
	
	@Override
	public void dispatch(double val1, double val2, double val3, double val4) { 
		lbl1.setText(Double.toString(val1));
		lbl2.setText(Double.toString(val2));
		lbl3.setText(Double.toString(val3));
		lbl4.setText(Double.toString(val4));
		
		switch (mode) {
		case LEARN:
			this.val1 = val1;
			this.val2 = val2;
			this.val3 = val3;
			this.val4 = val4;
			break;
		case CLASSIFY:
			classifier.classify(val1, val2, val3, val4);
			break;
		default:
			break;
		}
	}
	
	private void takeSamples() {
		int delay = 200;
		int nrOfSamples = Integer.parseInt(txtNrOfSamples.getText());
		for(int i = 1; i <= nrOfSamples; i++) {
			SchedulerUtils.scheduleRunOnce(INITIAL_DELAY + delay * i, 
					new SampleTakerRunnable(i));
		}
	}
	
	private class SampleTakerRunnable implements Runnable {
		private int sampleNr;

		public SampleTakerRunnable(int sampleNr) {
			this.sampleNr = sampleNr;
		}
		
		@Override
		public void run() {
			lblProgress.setText(Integer.toString(sampleNr) + " / " + txtNrOfSamples.getText());
			int sampleClass = currentLearningClass;
			learn(sampleClass, val1, val2, val3, val4);
		}
	}
	
	private void learn(int sampleClass, double val1, double val2, double val3, double val4) {
		double[] arr = new double[]{sampleClass, val1, val2, val3, val4};
		learning.add(arr);
	}
	
	private void saveLearnedData(File file) throws IOException {
		BufferedWriter outputWriter = null;
		outputWriter = new BufferedWriter(new FileWriter(file));
		for(double[] arr : learning) {
			for (int i = 0; i < arr.length ; i++) {
				String toWrite = "";
				if(i == 0) {
					toWrite = Integer.toString((int)arr[i]); //class
				} else {
					toWrite = Double.toString(arr[i]); //values
				}
				if(!(i == arr.length -1)) {
					toWrite = toWrite + ", ";
				}
				outputWriter.write(toWrite);				
			}
			outputWriter.newLine();
		}
		outputWriter.flush();  
		outputWriter.close();  
		
		JOptionPane.showMessageDialog(learnerWindow, "Data saved", title, JOptionPane.INFORMATION_MESSAGE);
	}


}
