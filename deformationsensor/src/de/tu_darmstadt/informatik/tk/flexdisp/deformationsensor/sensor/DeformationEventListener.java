package de.tu_darmstadt.informatik.tk.flexdisp.deformationsensor.sensor;

/**
 * Interface for deformation event listeners.<br>
 * Instances of this class can be used as discrete and continuous listeners.
 * @author Michael Winkler
 */
public interface DeformationEventListener {
	public void newDeformationEvent(DeformationEvent deformationEvent);
}
