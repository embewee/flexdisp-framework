package de.tu_darmstadt.informatik.tk.flexdisp.deformationsensor.sensor;

/**
 * An Exception which may be thrown by classes which are used on other projects.
 * This class may hide more concrete exceptions from the gnu.io package.
 * @author Michael Winkler
 */
public class ArduinoException extends Exception {
	
	private static final long serialVersionUID = 1L;

	/**
	 * @param message String to embed.
	 */
	public ArduinoException(String message) {
		super(message);
	}
	
	/**
	 * @param e Exception to embed.
	 */
	public ArduinoException(Exception e) {
		super(e);
	}
	
	/**
	 * @param e Exception to embed.
	 * @param message String to embed.
	 */
	public ArduinoException(Exception e, String message) {
		super(message, e);
	}

}
