package de.tu_darmstadt.informatik.tk.flexdisp.deformationsensor.sensor;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;

import net.sf.javaml.classification.Classifier;
import net.sf.javaml.classification.KNearestNeighbors;
import net.sf.javaml.core.Dataset;
import net.sf.javaml.core.DenseInstance;
import net.sf.javaml.tools.data.FileHandler;
import de.tu_darmstadt.informatik.tk.flexdisp.datastructures.EqualValueRing;
import de.tu_darmstadt.informatik.tk.flexdisp.datastructures.IntegerResult;

/**
 * k-nearest-neighbour based deformation gesture classifier. 
 * Based on the Java Machine Learning (JavaML) library kNN-Algorithm.
 * For JavaML see <a href="http://java-ml.sourceforge.net/">
 * http://java-ml.sourceforge.net/</a>.
 * @author Michael Winkler
 */
public class DeformationKnnClassifier extends DeformationInputDispatcher{
	private Classifier knn;
	private EqualValueRing ring;
	private HashSet<DeformationEventListener> discretelisteners;
	private HashSet<DeformationEventListener> continuousListeners;
	
	/**
	 * Creates a new knn-classifier based on the learned data in the given data set.
	 * @param learnedDataSet File representing the learned data set.
	 * @param kValue k-nearest-neighbour-classifier k-value.
	 * @param smoothing Number of consecutive equal classifications needed to trigger an event.
	 * @throws IOException
	 */
	public DeformationKnnClassifier(File learnedDataSet, int kValue, int smoothing) throws IOException {
		ring = new EqualValueRing(smoothing);
		discretelisteners = new HashSet<>();
		continuousListeners = new HashSet<>();
		
		Dataset data = FileHandler.loadDataset(learnedDataSet, 0, ",");
		knn = new KNearestNeighbors(kValue);
		knn.buildClassifier(data);
	}
	
	/**
	 * Registers the given deformation event listener as listener for discrete events.
	 * Discrete events are only triggered once! <p>
	 * For continuous triggering during the whole time of gesture [TODO: comment] 
	 * @param e DeformationEventListener
	 * @return boolean result from HashSet add action.
	 */
	public boolean addDiscreteDeformationEventListener(DeformationEventListener e) {
		return discretelisteners.add(e);
	}

	/**
	 * [TODO: comment] 
	 * @param e
	 * @return
	 */
	public boolean addContinuousDeformationEventListener(DeformationEventListener e) {
		return continuousListeners.add(e);
	}

	/**
	 * [TODO: comment] 
	 * @param e
	 * @return
	 */
	public boolean removeDiscreteDeformationEventListener(DeformationEventListener e) {
		return discretelisteners.remove(e);
	}
	
	/**
	 * [TODO: comment] 
	 * @param e
	 * @return
	 */
	public boolean removeContinuousDeformationEventListener(DeformationEventListener e) {
		return continuousListeners.remove(e);
	}
	
	@Override
	public void dispatch(double val1, double val2, double val3, double val4) {
		classify(val1, val2, val3, val4);
	}
	
	/**
	 * Classifies the given data from bends sensors into a deformation gesture and triggers the 
	 * appropriate event.
	 * @param val1 Value from bend sensor 1.
	 * @param val2 Value from bend sensor 2.
	 * @param val3 Value from bend sensor 3.
	 * @param val4 Value from bend sensor 4.
	 */
	public void classify(double val1, double val2, double val3, double val4) {
		double[] arr = new double[] {val1, val2, val3, val4};
		DenseInstance instance = new DenseInstance(arr);
		Object predictedClassValue = knn.classify(instance);
		Integer predictedClass = Integer.parseInt((String) predictedClassValue);
		
		ring.stuff(predictedClass);
		IntegerResult result = ring.allEqual();
		if (result.isSuccess()) {
			Integer proofedClass = result.getValue();
			determineEvent(proofedClass);
		}
	}
	
	private int oldValue = 0;
	/**
	 * Triggers information about continuous and discrete bend events.
	 * @param classifiedClass int label of class as according to DeformationEvent.Type.
	 */
	private void determineEvent(int classifiedClass) {
		if(classifiedClass != oldValue) {
			DeformationEvent.Type type = determineEventType(classifiedClass);
			informListeners(discretelisteners, type);
			informListeners(continuousListeners, type);
			oldValue = classifiedClass;
		} else if (classifiedClass != 0) {
			DeformationEvent.Type type = determineEventType(classifiedClass);
			informListeners(continuousListeners, type);			
		}
	}

	/**
	 * Resolves class integer label to deformation event type
	 * according to DeformationEvent.Type.
	 * @param classifiedClass
	 * @return
	 */
	private DeformationEvent.Type determineEventType(int classifiedClass) {
		switch(classifiedClass) {
			case 0: return DeformationEvent.Type.RESTING;
			case 1: return DeformationEvent.Type.MIDDLE_UP;
			case 2: return DeformationEvent.Type.MIDDLE_DOWN;
			case 3: return DeformationEvent.Type.UPPER_RIGHT_UP;
			case 4: return DeformationEvent.Type.UPPER_RIGHT_DOWN;
			case 5: return DeformationEvent.Type.BOTTOM_RIGHT_UP;
			case 6: return DeformationEvent.Type.BOTTOM_RIGHT_DOWN;
			case 7: return DeformationEvent.Type.UPPER_LEFT_UP;
			case 8: return DeformationEvent.Type.UPPER_LEFT_DOWN;
			case 9: return DeformationEvent.Type.TWIST_RIGHT_UP;
			case 10: return DeformationEvent.Type.TWIST_RIGHT_DOWN;
			case 11: return DeformationEvent.Type.EDGE_RIGHT_UP;
			case 12: return DeformationEvent.Type.EDGE_RIGHT_DOWN;
			case 13: return DeformationEvent.Type.EDGE_LEFT_UP; 
			case 14: return DeformationEvent.Type.EDGE_LEFT_DOWN;
			default: return null;
		}
		
	}
	
	/**
	 * 
	 * @param listeners Set of listeners to be informed.
	 * @param type DeformationEvent.Type
	 */
	private void informListeners(HashSet<DeformationEventListener> listeners, DeformationEvent.Type type) {
		for(DeformationEventListener l : listeners) {
			l.newDeformationEvent(new DeformationEvent(type));
		}
	}

}
