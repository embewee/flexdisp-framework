package de.tu_darmstadt.informatik.tk.flexdisp.mapsapp;

import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;

import com.lynden.gmapsfx.javascript.event.MapStateEventType;
import com.lynden.gmapsfx.javascript.object.GoogleMap;
import com.lynden.gmapsfx.javascript.object.LatLong;
import com.lynden.gmapsfx.javascript.object.MapOptions;
import com.lynden.gmapsfx.javascript.object.MapTypeIdEnum;
import com.lynden.gmapsfx.map.GoogleMapView;
import com.lynden.gmapsfx.map.MapComponentInitializedListener;

import de.tu_darmstadt.informatik.tk.flexdisp.deformationsensor.sensor.DeformationEvent;
import de.tu_darmstadt.informatik.tk.flexdisp.deformationsensor.sensor.DeformationEventListener;
import de.tu_darmstadt.informatik.tk.flexdisp.launchable.Context;
import de.tu_darmstadt.informatik.tk.flexdisp.launchable.Launchable;
import de.tu_darmstadt.informatik.tk.flexdisp.launchable.utils.SchedulerUtils;

/**
 * A simple Google Maps based map application. 
 * @author Based on the fx-g-maps project and code from 
 * Rob Terpilowski and Geoff Capper.
 * Adapted by Michael Winkler
 *
 */
public class MapApp implements Launchable, MapComponentInitializedListener {

	private GoogleMapView mapView;
	private GoogleMap map;
	
	private double  DARMSTADT_LAT = 49.87760;
	private double  DARMSTADT_LONG = 8.654929;
	private int HOME_ZOOM = 12;
	
	private DeformationEventListener continuousListener;
	
	private boolean scrollingBlocked;
	private int BLOCK_TIME = 1000;
	
	private Context context;
	
	private Pane toolbar;
	
	public MapApp() {
		block();
	}

	@Override
	public void mapInitialized() {
		// Once the map has been loaded by the Webview, initialize the map details.
		MapOptions options = new MapOptions();
		options.center(new LatLong(DARMSTADT_LAT, DARMSTADT_LONG)).mapMarker(true).zoom(HOME_ZOOM)
				.overviewMapControl(false).panControl(false)
				.rotateControl(false).scaleControl(false)
				.streetViewControl(false).zoomControl(false)
				.mapType(MapTypeIdEnum.ROADMAP);

		map = mapView.createMap(options);
		
		registerMapListeners();
	}
	
	private void zoomIn() {
		if(!scrollingBlocked) {
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					map.zoomIn();
					block();
				}
			});			
		}
	}
	
	private void zoomOut() {
		if(!scrollingBlocked) {
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					map.zoomOut();
					block();
				}
			});			
		}
	}
	
	private DeformationEvent.Type lastDefEvent = null;
	private double factor = 1.0; 
	
	
	private void checkLast(DeformationEvent.Type d) {
		if (lastDefEvent == d) {
			factor *= 1.1;
		} else {
			factor = 1.0;
			lastDefEvent = d;
		}
	}
	
	
	private void left() {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				LatLong ll = map.getCenter();
				LatLong next = ll.getDestinationPoint(270, 25 * factor);
				map.setCenter(next);
			}
		});	
	}
	
	private void right() {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				LatLong ll = map.getCenter();
				LatLong next = ll.getDestinationPoint(90, 25 * factor);
				map.setCenter(next);
			}
		});	
	}
	
	public synchronized void block() {
		scrollingBlocked = true;
		SchedulerUtils.scheduleRunOnce(BLOCK_TIME, new Runnable() {
			@Override
			public void run() {
				scrollingBlocked = false;
			}
		});
	}
	
	private void registerMapListeners() {
		map.zoomProperty().addListener(
			(ObservableValue<? extends Number> obs, Number o, Number n) -> {
				System.out.println("ZOOMPROP=" + n.toString());
		});

		map.addStateEventHandler(MapStateEventType.idle, () -> {
			System.out.println("idle...");
		});

		map.addStateEventHandler(MapStateEventType.tilesloaded, () -> {
			System.out.println("tilesloaded");
		});
	}

	@Override
	public void enterStage(Context context) {
		this.context = context;
		scrollingBlocked = false;
		context.addCSS(getClass().getResource("Buttons.css").toExternalForm());
		mapView = new GoogleMapView(); // mapComponant is_an AnchorPane
		mapView.addMapInializedListener(this);
		
		setMapBendSensorDispatcher();
		
		toolbar = new HBox();
		Image homeImage = new Image(getClass().getResourceAsStream("home.png"));
		ImageView home = new ImageView(homeImage);
		home.setOnMouseClicked(e -> {
			home();
		});
		toolbar.getChildren().add(home);
		context.addToolbarElement(toolbar);
	}
	
	private void home() {
		map.setCenter(new LatLong(DARMSTADT_LAT, DARMSTADT_LONG));
		map.setZoom(HOME_ZOOM);
	}
	
	private void setMapBendSensorDispatcher() {
		continuousListener = new DeformationEventListener() {
			@Override
			public void newDeformationEvent(DeformationEvent deformationEvent) {
				switch (deformationEvent.getType()) {
				case MIDDLE_UP:
					zoomOut();
					checkLast(deformationEvent.getType());
					break;
				case MIDDLE_DOWN:
					zoomIn();
					checkLast(deformationEvent.getType());
					break;
				case EDGE_LEFT_DOWN:
					left();
					checkLast(deformationEvent.getType());
					break;
				case EDGE_RIGHT_DOWN:
					right();
					checkLast(deformationEvent.getType());
					break;
				case RESTING:
					checkLast(deformationEvent.getType());
					break;
				default:
					break;
				}
			}
		};
		context.getDeformationSensor().addContinuousDeformationEventListener(continuousListener);
	}
	
	@Override
	public void quitStage() {
		map = null;
		context.getDeformationSensor().removeContinuousDeformationEventListener(continuousListener);
		context.removeCSS(getClass().getResource("Buttons.css").toExternalForm());
		context.removeToolbarElement(toolbar);
	}
	
	@Override
	public String getAppName() {
		return "Karten";
	}

	@Override
	public Pane getAppPane() {
		return mapView;
	}

	@Override
	public Image getAppImage() {
		return new Image(getClass().getResourceAsStream("map.png"));
	}
}
