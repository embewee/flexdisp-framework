package de.tu_darmstadt.informatik.tk.flexkit.jconnector;

public class ConnectorException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ConnectorException(String message) {
		super(message);
	}
}
