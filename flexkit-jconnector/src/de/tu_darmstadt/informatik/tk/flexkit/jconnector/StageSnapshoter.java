package de.tu_darmstadt.informatik.tk.flexkit.jconnector;

import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

import javafx.application.Platform;
import javafx.stage.Stage;

//TODO: Logging instead of stacktrace printing

/**
 * Combines functionality to continuously and on-demand send updated to a connected FlexKit device (flexible display).
 */
public class StageSnapshoter extends Snapshoter {
	
	private MonochromeFlexKitConnector connector; 
	private Stage stage;
	
	//Continuous execution
	private boolean continuous_fullUpdateRequested;
	
	private Future<?> currentRunner;
	
	/**
	 * Creates a new SceneSnapshoter. A SceneSnapshoter consumes a connected MonochromeFlexKitConnector and the observed scene.<br>
	 * <b>Initial values for display refresh types are set.</b> Call methods <code>setRefreshTypesForManualUpdates</code> and 
	 * <code>setRefreshTypesForContinuousUpdates</code> to set custom refresh types.
	 * @param connector Java FlexKit Connector. Must already be connected. 
	 * @param scene
	 * @throws ConnectorException If connector is not connected.
	 */
	public StageSnapshoter(MonochromeFlexKitConnector connector) throws ConnectorException {
		this.connector = connector;
		
		currentRunner = null;
		
		//initial values
		setRefreshTypesForManualUpdates(1, 19);
		
		setRefreshTypesForContinuousUpdates(1, 19);
		setInitialDelay(1000);
		setPeriod(1000);
		
		//initial update during continuous execution should be 
		continuous_fullUpdateRequested = true;
	}
	
	@Override
	public void setPeriod(int period) {
		super.setPeriod(period);
		if(currentRunner != null) {
			currentRunner.cancel(false);
			currentRunner = schedule(new Runner(), 0, getPeriod());
		}
	}
	
	/**
	 * Sets the snapshoters stage to the given one. The given stage will be updated in future display updates.  
	 * @param stage
	 */
	public void setStage(Stage stage) {
		this.stage = stage;
	}
	
	public MonochromeFlexKitConnector getConnector() {
		return connector;
	}
	
	//manual snapshooting
	
	/**
	 * Updates the display with the set display refresh type for manual partial updates.
	 */
	public void manualPartialDisplayUpdate() {
		try {
			connector.send(stage.getScene(), getManualUpdateRefreshType());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ConnectorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Updates the display with the set display refresh type for manual full updates.
	 */
	public void manualFullDisplayUpdate() {
		try {
			connector.send(stage.getScene(), getManualFullUpdateRefreshType());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ConnectorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Updates the display with the given display refresh type.
	 */
	public void manualDisplayUpdate(int refreshType) {
		try {
			connector.send(stage.getScene(), refreshType);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ConnectorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	// continuous snapshooting
	
	/**
	 * Requests a total display update in the next continuously executed cycle.
	 * @param type Refresh type
	 */
	public void requestFullUpdate() {
		continuous_fullUpdateRequested = true;
	}

	public void setOperationMode(OperationMode operationMode) {
		connector.setOperationMode(operationMode);
	}
	
	public OperationMode getOperationMode() {
		return connector.getOperationMode();
	}
	
	public void setDitherMatrix(int[][] ditherMatrix) {
		connector.setDitherMatrix(ditherMatrix);
	}
	
	public int[][] getDitherMatrix() {
		return connector.getDitherMatrix();
	}
	
	/**
	 * Start continuous transmission of snapshots.<br>
	 */
	public void start() {
		if(currentRunner != null) {
			return;
		}
		
		Runner r = new Runner();
		currentRunner = schedule(r, getInitialDelay(), getPeriod());
	}
	
	/**
	 * Pauses the continuous transmission of snapshots
	 */
	public void stop() {
		if(currentRunner != null) {
			currentRunner.cancel(false);
			currentRunner = null;
		}
	}
	
//	/**
//	 * Resumes the continuous transmission of snapshots
//	 */
//	public void resume() {
//		Runner r = new Runner();
//		currentRunner = schedule(r, 0, getPeriod());
//	}
	
	/**
	 * Returns true, if the SceneSnapshoter is continuously taking snapshots and sending snapshots to the device, false otherwise.<br>
	 * @return boolean
	 */
	public boolean isRunning() {
		return (currentRunner != null);
	}
	
	private static Future<?> schedule(Runnable runnable, long initialDelay, long period) {
		ScheduledExecutorService scheduledExecutorService = Executors.newSingleThreadScheduledExecutor(new ThreadFactory() {
	        @Override
	        public Thread newThread(Runnable r) {
	            Thread thread = new Thread(r);
	            thread.setDaemon(true);
	            return thread;
	        }
	    });
		return scheduledExecutorService.scheduleWithFixedDelay(runnable, initialDelay, period, TimeUnit.MILLISECONDS);		
	}	
	
	class Runner implements Runnable {
	    @Override
	    public void run() {
	        final Runnable runnable = new Runnable() {
	            @Override
	            public void run() {
	                try {
	                	if(continuous_fullUpdateRequested) {
	                		connector.send(stage.getScene(), getContinuousFullUpdateRefreshType());
	                		continuous_fullUpdateRequested = false;
	                	} else {
	                		connector.send(stage.getScene(), getContinuousUpdateRefreshType());
	                	}
	                	
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (ConnectorException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	            }
	        };
	        Platform.runLater(runnable);
	    }
	};
}

