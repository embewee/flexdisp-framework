package de.tu_darmstadt.informatik.tk.flexkit.jconnector;

public enum OperationMode {
	NORMAL,
	ROTATE,
	DITHER,
	ROTATE_DITHER;
}
