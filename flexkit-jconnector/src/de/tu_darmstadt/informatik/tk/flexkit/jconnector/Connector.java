package de.tu_darmstadt.informatik.tk.flexkit.jconnector;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class Connector {
	private String connectToIP;
	private int imagePort;
	private int controlPort;
	private int plScreenWidth;
	private int plScreenHeight;
	private int timeout;
	//private byte[] refreshType;
	private boolean connected;
	private Socket imageSocket;
	private DataOutputStream imageDataOutputStream;
	private Socket controlSocket;
	private DataOutputStream controlDataOutputStream;
	
	private boolean dryRun;
	
	public Connector() {
		connected = false;
		//refreshType = new byte[]{(byte) 5};
		timeout = 5000;
		dryRun = false;
	}
	
//	public int getRefreshType() {
//		return uB(refreshType[0]);
//	}
//	
//	/**
//	 * If you want it to be a �partial refresh�, leave it as that byte. 
//	 * If you want it to be a �full� (aka clear screen/flash) refresh, 
//	 * append a 9 to the number (i.e. refresh type 4, partial = 4; 
//	 * refresh type 0, full = 09; refresh type 5, full = 59).
//	 */
//	public void setRefreshType(int type) {
//		refreshType = new byte[]{(byte) type};
//	}
	
	/**
	 * Set hardware display resolution
	 * @param width int
	 * @param height int
	 */
	public void setHardwareDeviceSize(int width, int height) {
		plScreenWidth = width;
		plScreenHeight = height;
	}
	
	/**
	 * 
	 * @return
	 */
	public int getWidth() {
		return plScreenWidth;
	}
	
	/**
	 * 
	 * @return
	 */
	public int getHeight() {
		return plScreenHeight;
	}
	
	/**
	 * 
	 * @return int (DisplayWidht * DisplayHeight)
	 */
	public int getImageArrayLength() {
		return plScreenWidth * plScreenHeight;
	}
	
	/**
	 * 
	 * @return String IP
	 */
	public String getIP() {
		return connectToIP;
	}
	
	/**
	 * Device IP address.
	 * @param ip String
	 */
	public void setIP(String ip) {
		this.connectToIP = ip;
	}
	
	/**
	 * 
	 * @return
	 */
	public int getImagePort() {
		return imagePort;
	}

	/**
	 * 
	 * @param imagePort
	 */
	public void setImagePort(int imagePort) {
		this.imagePort = imagePort;
	}

	/**
	 * 
	 * @return
	 */
	public int getControlPort() {
		return controlPort;
	}

	/**
	 * 
	 * @param controlPort
	 */
	public void setControlPort(int controlPort) {
		this.controlPort = controlPort;
	}

	/**
	 * 
	 * @return
	 */
	public int getTimeout() {
		return timeout;
	}

	/**
	 * 
	 * @param timeout IP connection timeout [milliseconds]. 
	 */
	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}
	
	/**
	 * If dry is true, the connector does not connect and does not send any data.
	 * @param dry boolean
	 */
	public void dry(boolean dry) {
		this.dryRun = dry;
	}

	/**
	 * 
	 * @throws UnknownHostException
	 * @throws IOException
	 * @throws ConnectorException
	 */
	public void connect() throws UnknownHostException, IOException, ConnectorException {
		if(dryRun) {
			connected = true;
		} else if(!connected) {
			imageSocket = new Socket();
			imageSocket.connect(new InetSocketAddress(getIP(), getImagePort()), getTimeout());
			OutputStream imageOutputStream = imageSocket.getOutputStream(); 
		    imageDataOutputStream = new DataOutputStream(imageOutputStream);
		    
		    controlSocket = new Socket();
			controlSocket.connect(new InetSocketAddress(getIP(), getControlPort()), getTimeout());
			OutputStream controlOuputStream = controlSocket.getOutputStream();
			controlDataOutputStream = new DataOutputStream(controlOuputStream);
		
			connected = true;
		} else {
			throw new ConnectorException("Already connected to IP: " + connectToIP);
		}
	}
	
	/**
	 * 
	 * @return boolean
	 */
	public boolean isConnected() {
		return connected;
	}
	
	/**
	 * 
	 * @throws IOException
	 * @throws ConnectorException
	 */
	public void disconnect() throws IOException, ConnectorException {
		if(dryRun) {
			connected = false;
		} else if(connected) {
			imageSocket.close();
			controlSocket.close();
			
			connected = false;
		} else {
			throw new ConnectorException("Not connected.");
		}
	}
	
	/**
	 * Sends an array of bytes to the connected device.<br>
	 * If you want it to be a �partial refresh�, leave it as that byte. 
	 * If you want it to be a �full� (aka clear screen/flash) refresh, 
	 * append a 9 to the number (i.e. refresh type 4, partial = 4; 
	 * refresh type 0, full = 09; refresh type 5, full = 59).
	 * @param imageArray
	 * @param refreshType
	 * @throws IOException
	 * @throws ConnectorException
	 */
	public void send(byte[] imageArray, int refreshType) throws IOException, ConnectorException {
		if(dryRun) {
			return;
		}
		
		if(imageArray.length != getImageArrayLength()) {
			throw new ConnectorException("Number of bytes of image array must be " + getImageArrayLength() + 
					", but was only " + imageArray.length + ".");
		}
		
		if(connected) {
			byte[] refreshTypeByte = new byte[]{(byte) refreshType};
			
			imageDataOutputStream.write(imageArray);
		    imageDataOutputStream.flush();
			controlDataOutputStream.write(refreshTypeByte);
			controlDataOutputStream.flush();
		} else {
			throw new ConnectorException("Not connected.");
		}
	}
	
	/**
	 * Returns the unsigned byte value of given byte
	 * @param b
	 * @return
	 */
	public static int uB(byte b) {
		return b & 0xFF;
	}
}
