package de.tu_darmstadt.informatik.tk.flexkit.jconnector;

import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.File;
import java.io.IOException;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.Scene;
import javafx.scene.image.WritableImage;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

import org.apache.commons.lang3.ArrayUtils;

public class MonochromeFlexKitConnector extends Connector {
	private OperationMode operationMode;
	private int[][] ditherMatrix;
	
	public MonochromeFlexKitConnector(OperationMode mode) {
		super();
		
		this.operationMode = mode;
		ditherMatrix = NoDither;
		
		//Resolution of plastic logic flexible display (landscape)
		setHardwareDeviceSize(1280, 960); //FIXME: nach config

		//FlexKit connection properties
		setIP("192.168.1.10"); //FIXME
		setImagePort(50000); //FIXME
		setControlPort(50001); //FIXME
	}
	
	public OperationMode getOperationMode() {
		return operationMode;
	}
	
	public void setOperationMode(OperationMode operationMode) {
		this.operationMode = operationMode;
	}
	
	public void setDitherMatrix(int[][] ditherMatrix) {
		this.ditherMatrix = ditherMatrix;
	}
	
	public int[][] getDitherMatrix() {
		return ditherMatrix;
	}
	
	public void send(Scene scene, int refreshType) throws IOException, ConnectorException {
		BufferedImage grayImage = toGray(sceneToBufferedImage(scene));
		processBufferedImage(grayImage, refreshType);
	}
	
	public void send(JFrame frame, int refreshType) throws IOException, ConnectorException {
		BufferedImage grayImage = toGray(JFrameToBufferedImage(frame));
		processBufferedImage(grayImage, refreshType);
	}
	
	public void send(File imageFile, int refreshType) throws IOException, ConnectorException {
		BufferedImage grayImage = toGray(ImageIO.read(imageFile));
		processBufferedImage(grayImage, refreshType);
	}
	
	private void processBufferedImage(BufferedImage grayImage, int refreshType) throws IOException, ConnectorException {
		switch(operationMode) {
		case NORMAL:
			send(bufferedImageToByteArray(grayImage), refreshType);
			break;
			
		case ROTATE:
			send(rotate(bufferedImageToByteArray(grayImage)), refreshType);
			break;
			
		case DITHER:
			send(bufferedImageToByteArray(orderedDither(grayImage)), refreshType);
			break;
			
		case ROTATE_DITHER:
			send(rotate(bufferedImageToByteArray(orderedDither(grayImage))), refreshType);
			break;
		
		default:
			throw new ConnectorException("Invalid mode: " + operationMode);
		}
	}
	
	private BufferedImage sceneToBufferedImage(Scene scene) {
		WritableImage writableImage = new WritableImage(
				scene.widthProperty().intValue(), scene.heightProperty().intValue());
		WritableImage snapshot = scene.snapshot(writableImage);
		BufferedImage bufferedImage = SwingFXUtils.fromFXImage(snapshot, null);
		return bufferedImage;
	}
	
	private BufferedImage JFrameToBufferedImage(JFrame frame) {
		BufferedImage bufferedImage = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_RGB);
		Graphics2D graphics2d = (Graphics2D) bufferedImage.getGraphics();
		frame.paint(graphics2d);
		return bufferedImage;
	}
	
	private BufferedImage toGray(BufferedImage colorBufferedImage) {
		BufferedImage grayImage = new BufferedImage(colorBufferedImage.getWidth(), colorBufferedImage.getHeight(), BufferedImage.TYPE_BYTE_GRAY);
		Graphics2D graphics2d = grayImage.createGraphics();
		
		
		
		graphics2d.setRenderingHint(
		        RenderingHints.KEY_DITHERING, 
		        RenderingHints.VALUE_DITHER_ENABLE);
		
		
		
		
		graphics2d.drawImage(colorBufferedImage, 0, 0, null);
		return grayImage;
	}
	
	private byte[] rotate(byte[] input) {
		int length = input.length;
		byte[] output = new byte[length];
		int outputIndex = 0;
		int height = getHeight();
		for(int offset = 0; offset < height; offset++) {
			int width = getWidth();
			for(int w = width -1; w >= 0; w--) {
				//int imageArrayLength = getImageArrayLength();
				//int sourceIndex = w * imageArrayLength + offset;
				int sourceIndex = w * height + offset;
				byte val = input[sourceIndex];
				output[outputIndex] = val;
				outputIndex++;
			}
		}
		
		return reverse(output);
	}
	
	private byte[] reverse(byte[] arr) {
		ArrayUtils.reverse(arr);
		return arr;
	}
	
	private byte[] bufferedImageToByteArray(BufferedImage grayImage) {
		DataBufferByte bytes = (DataBufferByte) grayImage.getRaster().getDataBuffer();
		byte[] imageByteArray = bytes.getData();
		return imageByteArray;
	}

	
	//---------------------------------------
	//TODO: 
	
	/**
	 * Dither matrix from AForge.NET
	 * http://www.aforgenet.com/framework/docs/html/84a17bb9-0636-935f-9846-58b3ca0c9c55.htm
	 */
	public final static int[][] NoDither = new int[][]
		{
			{1}};
	
	/**
	 * Dither matrix from AForge.NET
	 * http://www.aforgenet.com/framework/docs/html/84a17bb9-0636-935f-9846-58b3ca0c9c55.htm
	 */
	public final static int[][] BayerDither = new int[][]
		{
			{   0, 192,  48, 240 },
			{ 128,  64, 176, 112 },
		    {  32, 224,  16, 208 },
		    { 160,  96, 144,  80 }
		};
	
	/**
	 * Modified bayer dither matrix, tries to fit the flexible display's gray scale.
	 */
	public final static int[][] FlexModBayerDither = new int[][]
		{
			{   0, 221,  51, 255 },
			{ 153,  68, 204, 119 },
		    {  34, 136,  17, 238 },
		    { 187,  102, 170, 85 }
		};
	
	/**
	 * TODO: ENTNOMMEN AUS IMAGETOOLS VON
	 * http://tams-www.informatik.uni-hamburg.
	 * de/lehre/2004ss/vorlesung/medientechnik/material/ImageTools.java
	 * 
	 * 
	 * @param image
	 * @param dither
	 * @return
	 */
	private BufferedImage orderedDither(BufferedImage image) {
		int w = image.getWidth();
		int h = image.getHeight();

		BufferedImage output = new BufferedImage(w, h, BufferedImage.TYPE_BYTE_GRAY);
		Graphics2D graphics2d = output.createGraphics();
		graphics2d.drawImage(image, 0, 0, null);

		int dw = ditherMatrix.length;
		int dh = ditherMatrix[0].length;

		for (int y = 0; y < h; y++) { // top-down
			int dy = y % dh;

			for (int x = 0; x < w; x++) { // left-right

				int pixel = image.getRGB(x, y);
				int red = (pixel & 0x00FF0000) >> 16;
				int green = (pixel & 0x0000FF00) >> 8;
				int blue = (pixel & 0x000000FF);

				int dx = x % dw;
				int tmp = ditherMatrix[dy][dx];

				if (red >= tmp)
					red = 255;
				else
					red = 0;

				if (green >= tmp)
					green = 255;
				else
					green = 0;

				if (blue >= tmp)
					blue = 255;
				else
					blue = 0;

				output.setRGB(x, y, (red << 16) + (green << 8) + blue);
			}
		}
		return output;
	}
}