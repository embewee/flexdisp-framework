package de.tu_darmstadt.informatik.tk.flexkit.jconnector;

/**
 * Abstract super class for concrete implementations of Snapshoters, handling some basic stuff.<br>
 * <b>Call the applicable methods after instantiation of a concrete Snaphoter to set default values:</b><br>
 * <code>setRefreshTypesForManualUpdates(), setRefreshTypesForContinuousUpdates(), setInitialDelay(), setPeriod().</code>
 * @author Michael Winkler
 *
 */
public abstract class Snapshoter {

	//private Connector connector; 
	
	//Manual execution
	private int manual_updateRefreshType;
	private int manual_fullUpdateRefreshType;
	
	//Continuous execution
	private int continuous_updateRefreshType;
	private int continuous_fullUpdateRefreshType;
	private int continuous_initialDelay;
	private int continuous_period;
	
	/**
	 * Returns the Connector used in this Snapshoter.
	 * @return Connector
	 */
	public abstract Connector getConnector();
	
	/**
	 * Set refresh types for manual updates.
	 * @param partialUpdate
	 * @param fullUpdate
	 */
	public void setRefreshTypesForManualUpdates(int partialUpdate, int fullUpdate) {
		manual_updateRefreshType = partialUpdate;
		manual_fullUpdateRefreshType = fullUpdate;
	}
	
	/**
	 * Set refresh types for continuous updates.
	 * @param partialUpdate
	 * @param fullUpdate
	 */
	public void setRefreshTypesForContinuousUpdates(int partialUpdate, int fullUpdate) {
		continuous_updateRefreshType = partialUpdate;
		continuous_fullUpdateRefreshType = fullUpdate;
	}
	
	/**
	 * 
	 * @param delay int Initial delay [milliseconds]
	 */
	public void setInitialDelay(int delay) {
		continuous_initialDelay = delay;
	}
	
	/**
	 * 
	 * @param period int Time in [milliseconds] between continuous executions.<br>
	 * <b>Should not be smaller than 200 (max. 5 refreshes per second).</b>
	 */
	public void setPeriod(int period) {
		continuous_period = period;
	}
	
	public int getManualUpdateRefreshType() {
		return manual_updateRefreshType;
	}
	
	public int getManualFullUpdateRefreshType() {
		return manual_fullUpdateRefreshType;
	}
	
	public int getContinuousUpdateRefreshType() {
		return continuous_updateRefreshType;
	}
	
	public int getContinuousFullUpdateRefreshType() {
		return continuous_fullUpdateRefreshType;
	}
	
	/**
	 * 
	 * @return
	 */
	public int getInitialDelay() {
		return continuous_initialDelay;
	}
	
	/**
	 * 
	 * @return
	 */
	public int getPeriod() {
		return continuous_period;
	}
	
	//manual snapshooting
	
	/**
	 * Updates the display with the set display refresh type for manual partial updates.
	 */
	public abstract void manualPartialDisplayUpdate();
	
	/**
	 * Updates the display with the set display refresh type for manual full updates.
	 */
	public abstract void manualFullDisplayUpdate();
	
	/**
	 * Updates the display with the given display refresh type.
	 */
	public abstract void manualDisplayUpdate(int refreshType);
	
	// continuous snapshooting
	
	/**
	 * Requests a total display update in the next continuously executed cycle.
	 * @param type Refresh type
	 */
	public abstract void requestFullUpdate();

	/**
	 * Start continuous transmission of snapshots.<br>
	 */
	public abstract void start();
	
	/**
	 * Stops the continuous transmission of snapshots
	 */
	public abstract void stop();
	
//	/**
//	 * Resumes the continuous transmission of snapshots
//	 */
//	public abstract void resume();
	
	/**
	 * Returns true, if the SceneSnapshoter is continuously taking snapshots and sending snapshots to the device, false otherwise.<br>
	 * @return boolean
	 */
	public abstract boolean isRunning();
	
}
