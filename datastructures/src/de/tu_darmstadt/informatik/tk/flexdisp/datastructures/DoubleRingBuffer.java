package de.tu_darmstadt.informatik.tk.flexdisp.datastructures;

/**
 * Ring for <code>Double</code> numbers 
 */
public class DoubleRingBuffer extends NumberRingBuffer<Double>{

	/**
	 * 
	 * @param length int the ring's maximum size
	 */
	public DoubleRingBuffer(int length) {
		super(length);
	}

	@Override
	public double mean() {
		double sum = 0;
		for (int i = 0; i < getRingBuffer().size(); i++) {
			sum += getRingBuffer().get(i);
		}
		return (sum/getRingBuffer().size());
	}
	
	/**
	 * 
	 * @param newValue
	 * @param oldValue
	 * @return (newValue - oldValue) / oldValue
	 */
	public static double variation(double newValue, double oldValue) {
		// (neu - alt) / alt
		double diff =  newValue - oldValue;
		double var = (diff / oldValue);
		return var;
	}

}
