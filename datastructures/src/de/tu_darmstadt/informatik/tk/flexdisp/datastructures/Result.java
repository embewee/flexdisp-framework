package de.tu_darmstadt.informatik.tk.flexdisp.datastructures;

/**
 * Encapsulates a result value and a success information.
 * @author michael
 *
 * @param <T>
 */
public class Result<T> {

	private boolean success;
	private T value;
	
	/**
	 * Creates a new Result with given result value and success information
	 * @param success boolean
	 * @param value T the result value
	 */
	public Result(boolean success, T value) {
		this.success = success;
		this.value = value;
	}

	/**
	 * 
	 * @return boolean the success informations
	 */
	public boolean isSuccess() {
		return success;
	}

	/**
	 * 
	 * @return T the result value
	 */
	public T getValue() {
		return value;
	}
	
	
}
