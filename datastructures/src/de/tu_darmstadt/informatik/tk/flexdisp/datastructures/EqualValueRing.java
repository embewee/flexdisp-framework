package de.tu_darmstadt.informatik.tk.flexdisp.datastructures;

public class EqualValueRing extends RingBuffer<Integer> {

	/**
	 * 
	 * @param length int the ring's maximum size
	 */
	public EqualValueRing(int length) {
		super(length);
	}
	
	/**
	 * Checks, if all values inside the ring are of the same value. In this case,
	 * an IntegerResult is returned with success value <code>true</code> and the 
	 * actual value. 
	 * Otherwise an IntegerResult is returned with success value <code>false</code>. 
	 * @return Integer result
	 */
	public IntegerResult allEqual() {
		boolean result = true;
		int equalVal = get(0);
		for(int i = 1;  i < size(); i++) {
			result &= (get(i).equals(equalVal));
		}
		return new IntegerResult(result, equalVal);
	}


}
