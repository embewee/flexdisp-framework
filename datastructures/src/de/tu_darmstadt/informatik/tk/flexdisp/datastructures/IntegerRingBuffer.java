package de.tu_darmstadt.informatik.tk.flexdisp.datastructures;


/**
 * RingBuffer for Integer values of given length. 
 */
public class IntegerRingBuffer extends NumberRingBuffer<Integer>{
	
	/**
	 * 
	 * @param length int the ring's maximum size
	 */
	public IntegerRingBuffer(int length) {
		super(length);
	}
	
	public double mean() {
		double sum = 0;
		for (int i = 0; i < getRingBuffer().size(); i++) {
			sum += getRingBuffer().get(i);
		}
		return (sum/getRingBuffer().size());
	}

	/**
	 * 
	 * @param newValue
	 * @param oldValue
	 * @return (newValue - oldValue) / oldValue
	 */
	public static double variation(int newValue, double oldValue) {
		// (neu - alt) / alt
		double diff = (double) newValue - oldValue;
		return(diff / oldValue);
	}
}
