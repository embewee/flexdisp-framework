package de.tu_darmstadt.informatik.tk.flexdisp.datastructures;

/**
 * 
 * Abstract basic class for <code>Number</code>-rings
 * @param <T> T extends Number
 */
public abstract class NumberRingBuffer<T extends Number> extends RingBuffer<T>{
	
	public NumberRingBuffer(int length) {
		super(length);
	}
		
	/**
	 * 
	 * @return Mean of ring buffer: sum(ring buffer elements) / size(ring buffer)
	 */
	public abstract double mean();

	
}
