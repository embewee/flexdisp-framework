package de.tu_darmstadt.informatik.tk.flexdisp.datastructures;

/**
 * Encapsulates integer results and a success information.
 *
 */
public class IntegerResult extends Result<Integer>{

	/**
	 * 
	 * @param success boolean 
	 * @param value the result value
	 */
	public IntegerResult(boolean success, Integer value) {
		super(success, value);
	}

}
