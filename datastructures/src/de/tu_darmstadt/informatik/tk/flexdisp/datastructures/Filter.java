package de.tu_darmstadt.informatik.tk.flexdisp.datastructures;

public class Filter {
	
	private double lowerThreshold;
	private double upperThreshold;
	
	/**
	 * Creates a new Filter with given lower and upper thresholds.
	 * @param lowerThreshold
	 * @param upperThreshold
	 */
	public Filter(double lowerThreshold, double upperThreshold) {
		this.lowerThreshold = lowerThreshold;
		this.upperThreshold = upperThreshold;
	}
	
	/**
	 * If the given value is less-or-equal the lower threshold or 
	 * greater-or-equal the upper threshold, the given value is returned.
	 * Otherwise the given default value is returned.
	 * @param value The double value to check.
	 * @param defaultValue The default value.
	 * @return The given value or the given default value.
	 */
	public double filter(double value, double defaultValue) {
		if (outside(value))  {
			return value;
		} else {
			return defaultValue;
		}
	}
	
	/**
	 * If the given value is less-or-equal the lower threshold or 
	 * greater-or-equal the upper threshold, the given value is returned.
	 * Otherwise 0 is returned.
	 * @param value The double value to check.
	 * @return The given value or 0.
	 */
	public double filter(double value) {
		return filter(value, 0);
	}
	
	/**
	 * Checks, if the given value is less-or-equal the lower threshold or 
	 * greater-or-equal the upper threshold. In those cases true is returned, otherwise false.
	 * @param value The double value to check.
	 * @return (value <= lowerThreshold || value >= upperThreshold)
	 */
	public boolean outside(double value) {
		return (value <= lowerThreshold || value >= upperThreshold);
	}
	
	public double getLowerThreshold() {
		return lowerThreshold;
	}

	/**
	 * Sets the lower threshold for filtering. <br>
	 * See method <code>filter()</code>.
	 * @param lowerThreshold double
	 */
	public void setLowerThreshold(double lowerThreshold) {
		this.lowerThreshold = lowerThreshold;
	}

	public double getUpperThreshold() {
		return upperThreshold;
	}

	/**
	 * Sets the upper threshold for filtering.<br>
	 * See method <code>filter()</code>.
	 * @param upperThreshold double
	 */
	public void setUpperThreshold(double upperThreshold) {
		this.upperThreshold = upperThreshold;
	}	
}
