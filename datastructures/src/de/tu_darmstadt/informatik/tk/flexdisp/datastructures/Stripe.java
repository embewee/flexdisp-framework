package de.tu_darmstadt.informatik.tk.flexdisp.datastructures;

public class Stripe {
	
	/**
	 * Mean value over first <code>bufferLength</code> values
	 */
	private double idleMean;
	
	private IntegerRingBuffer buffer;
	private Filter filter;
	
	/**
	 * Creates a new stripe instance. 
	 * @param bufferLength
	 * @param filter
	 */
	public Stripe(int bufferLength, Filter filter) {
		this.buffer = new IntegerRingBuffer(bufferLength);
		this.filter = filter;
		idleMean = Double.NaN;
	}

	/**
	 * Returns the variation to the <b>idle mean</b> value. The idle mean is calculated from the 
	 * when the underlying ring buffer is filled. 
	 */
	public double stuff(Integer value) {
		if(!buffer.isFilled()) {
			//calibrate
			buffer.stuff(value);
		} else if(buffer.isFilled() && (Double.isNaN(idleMean))) {
			idleMean = buffer.mean();
		} 
		
		double var = variation(value, idleMean);
		double filtered = filter.filter(var);
		
		return filtered;
	}	
	
	/**
	 * @return Mean value over first <code>bufferLength</code> values
	 */
	public double getIdleMean() {
		return idleMean;
	}
	
	/**
	 * @return <code>true</code> if the stripe contains <code>bufferLength</code> values, <code>false</code> otherwise
	 */
	public boolean isFilled() {
		return buffer.isFilled();
	}
	
	/**
	 * @return <code>true</code> if the stripe does not contain any values, <code>false</code> otherwise
	 */
	public boolean isEmpty() {
		return buffer.isEmpty();
	}
	
	/**
	 * @return The number of values stored in this stripe. <b>This is not the max size of the buffer.</b>
	 */
	public int bufferSize() {
		return buffer.size();
	}
	
	/**
	 * Calculates the variation
	 * @param newValue
	 * @param oldValue
	 * @return (newValue - oldValue) / oldValue
	 */
	public static double variation(double newValue, double oldValue) {
		// (neu - alt) / alt
		double diff =  newValue - oldValue;
		double var = (diff / oldValue);
		return var;
	}
	
	/**
	 * Returns the filter set at instantiation.
	 * @return Filter
	 */
	public Filter getFilter() {
		return filter;
	}
}
