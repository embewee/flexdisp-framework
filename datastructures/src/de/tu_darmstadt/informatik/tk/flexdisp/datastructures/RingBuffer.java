package de.tu_darmstadt.informatik.tk.flexdisp.datastructures;

import org.apache.commons.collections4.queue.CircularFifoQueue;

/**
 * Represents a closed ring of objects with given length.
 * 
 * @param <T> Objects to be stored by this ring
 */
public abstract class RingBuffer<T extends Object> {
	private CircularFifoQueue<T> queue;
	
	/**
	 * 
	 * @param length int the ring's maximum size
	 */
	public RingBuffer(int length) {
		queue = new CircularFifoQueue<>(length);
	}
	
	/**
	 * Return the encapsulated <code>CircularFifoQueue</code>, which
	 * stores the objects 
	 * @return
	 */
	protected CircularFifoQueue<T> getRingBuffer() {
		return queue;
	}
	
	/**
	 * Returns the object at the given position
	 * @param index
	 * @return T The object at given index
	 */
	public T get(int index) {
		return queue.get(index);
	}
	
	
	public void clear() {
		queue.clear();
	}
	
	/**
	 * Returns the current size of this ring <br>
	 * <code>size</code> <= <code>maxSize</code>
	 * @return int
	 */
	public int size() {
		return queue.size();
	}
	
	/**
	 * Returns the maximal size of this ring. Equals the length parameter,
	 * passed by instantiation.
	 * @return int 
	 */
	public int maxSize() {
		return queue.maxSize();
	}
	
	/**
	 * Returns true, if the ring contains the maximal number of objects:<br>
	 * <code>(queue.size() == queue.maxSize())</code>
	 * @return
	 */
	public boolean isFilled() {
		return (queue.size() == queue.maxSize());
	}
	
	/**
	 * Returns true, if this ring does not contain any objects.
	 * @return boolean
	 */
	public boolean isEmpty() {
		return queue.isEmpty();
	}
	
	/**
	 * Adds an element to the ring. If the ring is full, adding new elements
	 * overwrites the elements already contained in the ring.
	 * @param element T
	 */
	public void stuff(T element) {
		queue.add(element);
	}
}
