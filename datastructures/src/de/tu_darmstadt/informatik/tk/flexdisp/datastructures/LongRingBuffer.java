package de.tu_darmstadt.informatik.tk.flexdisp.datastructures;

/**
 * Ring for <code>Long</code> numbers. 
 *
 */
public class LongRingBuffer extends NumberRingBuffer<Long>{

	/**
	 * 
	 * @param length int the ring's maximum size
	 */
	public LongRingBuffer(int length) {
		super(length);
	}

	@Override
	public double mean() {
		double sum = 0;
		for (int i = 0; i < getRingBuffer().size(); i++) {
			sum += getRingBuffer().get(i);
		}
		return (sum/getRingBuffer().size());
	}

	/**
	 * 
	 * @param newValue
	 * @param oldValue
	 * @return (newValue - oldValue) / oldValue
	 */
	public static double variation(long newValue, double oldValue) {
		// (neu - alt) / alt
		double diff =  newValue - oldValue;
		double var = (diff / oldValue);
		return var;
	}

}
