package de.tu_darmstadt.informatik.tk.flexdisp.launchable.listener;

import javafx.scene.image.Image;

/**
 * Listener, informed about title and icon changes for the global menu bar. 
 * @author Michael Winkler
 */

	public interface LaunchableNamingListener {
		
		/**
		 * Informs the global menu bar about a new title to be displayed.
		 * @param launchableName String
		 */
		public abstract void launchableNameChanged(String launchableName);
	
		/**
		 * Informs the global menu bar about a new icon to be displayed.
		 * @param launchableImage Image
		 */
		public abstract void launchableIconChanged(Image launchableImage);
}
