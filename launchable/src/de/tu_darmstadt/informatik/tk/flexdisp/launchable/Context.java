package de.tu_darmstadt.informatik.tk.flexdisp.launchable;

import java.util.Properties;
import java.util.Stack;

import javafx.application.Platform;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import de.tu_darmstadt.informatik.tk.flexdisp.deformationsensor.sensor.DeformationSensor;
import de.tu_darmstadt.informatik.tk.flexdisp.launchable.listener.ContextHistoryListener;
import de.tu_darmstadt.informatik.tk.flexdisp.launchable.listener.LaunchableNamingListener;
import de.tu_darmstadt.informatik.tk.flexdisp.launchable.listener.ModalMessageListener;
import de.tu_darmstadt.informatik.tk.flexdisp.launchable.listener.ToolbarElementsListener;
import de.tu_darmstadt.informatik.tk.flexkit.jconnector.StageSnapshoter;

/**
 * Class, which maintains the context of the application. It provides access to the snapshoter and
 * deformation sensor, lets Launchables enter and quit the Scene and shows message overlays.
 * <p>
 * Snapshoter, deformation sensor, context history listener, launchable naming listener,
 * and modal message listener must be set! 
 * 
 * @author Michael B. Winkler
 */
public class Context {
	private StageSnapshoter snapshoter;
	private DeformationSensor deformationSensor;
	private Scene scene;
	private Pane appStage;
	private Stack<Launchable> history;
	private Stack<Pane> messageStack;
	private ContextHistoryListener historyListener;
	private LaunchableNamingListener launchableNamingListener;
	private ToolbarElementsListener toolbarElementsListener;
	private Properties properties;
	private ModalMessageListener modalMessageListener;
	private boolean modalMessageIsShown = false;
	
	/**
	 * Initialized the surrounding app's context.
	 * @param scene The surrounding app's Scene.
	 * @param appStage The container to place Launchables in.
	 */
	public Context(Scene scene, Pane appStage) {
		this.scene = scene;
		this.appStage = appStage;
		snapshoter = null;
		deformationSensor = null;
		history = new Stack<>();
		messageStack = new Stack<>();
	}
	
	/**
	 * Steps back to the last shown <code>Launchable</code> app.
	 */
	public void back() {
		if (isShowingMessage()) {
			popMessage();
			return;
		}
		
		//do not remove last app (menuApp)
		if(history.size() <= 1) { 
			return;
		}
		
		//quit current
		Launchable appToQuit = history.pop();
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				appToQuit.quitStage();
				appStage.getChildren().remove(appToQuit.getAppPane());
			}
		});
		
		//launch previous
		Launchable next = history.peek();
		Context outer = this;
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				next.enterStage(outer);
				appStage.getChildren().add(next.getAppPane());
			}
		});
		
		informNamingListener();
		informHistoryListener();
	}

	/**
	 * Launches the provided <code>Launchable</code> app. 
	 * If a message is shown, the Launchable is started in background.
	 * @param app The Launchable to enter the stage
	 */
	public void launch(Launchable app) {
		//quit current
		if(!history.isEmpty()) {
			Launchable currentApp = history.peek();
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					currentApp.quitStage();
					appStage.getChildren().remove(currentApp.getAppPane());
				}
			});
		}
		
		//launch new one
		Context outer = this;
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				app.enterStage(outer);
				appStage.getChildren().add(app.getAppPane());
				if(isShowingMessage()) {
					//open app in background, if message is shown
					app.getAppPane().toBack();
				}
			}
		});
		history.push(app);
		
		informNamingListener();
		informHistoryListener();
		
		System.out.println("Launched " + app.getAppName());
	}
	
	private void informHistoryListener() {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				if(historyListener != null) {
					if(history.size() > 1) {
						historyListener.historyAvailable();
					} else if (!messageStack.isEmpty()) {
						historyListener.historyAvailable();
					} else if(history.size() == 1) {
						historyListener.historyEmpty();
					} else if(messageStack.isEmpty()) {
						historyListener.historyEmpty();
					}
				}				
			}
		});

	}
	
	private void informNamingListener() {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				if(launchableNamingListener != null) {
					Platform.runLater(new Runnable() {
						@Override
						public void run() {
							launchableNamingListener.launchableNameChanged(getCurrentLaunchableName());
							launchableNamingListener.launchableIconChanged(getCurrentLaunchableImage());
						}
					});
				}				
			}
		});
	}
	
	
	//Message overlays
	
	/**
	 * Pushes the given message pane onto the stage on top of the active Launchable and 
	 * blocks the launch of new apps until all message panes are removed.
	 * @param messagePane The pane to show on the stage on top of the ative app.
	 * @param modal If <code>true</code>, the global back button is blocked until this message pane is closed.
	 * @return <code>true</code>, if the message is shown. If the a modal message is shown
	 *  no other message can be pushed and <code>false</code> is returned.
	 */
	//FIXME: it should be possible to show error messages over modal overlays
	public boolean pushMessage(Pane messagePane, boolean modal) {
		if(modalMessageIsShown) {
			return false;
		}
		
		messageStack.push(messagePane);
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				appStage.getChildren().add(messagePane);
			}
		});

		informHistoryListener();
		
		if(modal) {
			informModalMessageListenerPushed();
		}
		
		return true;
	}
	
	/**
	 * Removes the topmost message pane from the stage.
	 */
	public void popMessage() {
		if(isShowingMessage()) {
			Pane pane = messageStack.pop();
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					appStage.getChildren().remove(pane);
				}
			});
			
			informHistoryListener();
			
			if(modalMessageIsShown) {
				informModalMessageListenerPopped();
			}
		}
	}
	
	private void informModalMessageListenerPushed() {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				if(modalMessageListener != null) {
					modalMessageIsShown = true;
					modalMessageListener.modalMessageEntered();
				}
			}
		});
	}
	
	private void informModalMessageListenerPopped() {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				if(modalMessageListener != null) {
					modalMessageListener.modalMessageQuitted();
					modalMessageIsShown = false;
				}
			}
		});
	}
	
	/**
	 * @return <code>true</code> if currently a message is shown, <code>false</code> otherwise.
	 */
	public boolean isShowingMessage() {
		return (!messageStack.isEmpty());
	}
	
	/**
	 * Adds styles described in a CSS file to the scene. The styles are applied for all currently shown affected UI elements. 
	 * @param resourceURL
	 */
	public void addCSS(String resourceURL) {
		scene.getStylesheets().add(resourceURL);
	}
	
	/**
	 * Removes styles described in a CSS file from the scene.
	 * @param resourceURL
	 */
	public void removeCSS(String resourceURL) {
		scene.getStylesheets().remove(resourceURL);
	}
	
	//Setter, Getter for class members, listeners, ...
	
	/**
	 * 
	 * @param snapshoter <code>StageSnapshoter</code>
	 */
	public void setSnapshoter(StageSnapshoter snapshoter) {
		this.snapshoter = snapshoter;
	}
	
	/**
	 * 
	 * @return The set <code>StageSnapshoter</code>
	 */
	public StageSnapshoter getSnapshoter() {
		return snapshoter;
	}
	
	/**
	 * 
	 * @param deformationSensor <code>DeformationSensor</code>
	 */
	public void setDeformationSensor(DeformationSensor deformationSensor) {
		this.deformationSensor = deformationSensor;
	}
	
	/**
	 * 
	 * @return The set <code>DeformationSensor</code>
	 */
	public DeformationSensor getDeformationSensor() {
		return deformationSensor;
	}
	
	/**
	 * Sets a Properties-Object which entries can be accessed through all partial applications by 
	 * the corresponding key.
	 * @param properties
	 */
	public void setProperties(Properties properties) {
		this.properties = properties;
	}
	
	/**
	 * Returns the value associated with the given key of null, if the key doesn't exist
	 * or no properties are set.
	 * @param key
	 * @return
	 */
	public String getProperty(String key) {
		if(properties != null) {
			return properties.getProperty(key);
		} 
		return null;
	}

	
	/**
	 * Adds a Node to the global toolbar.
	 * @param node Node to add to the global toolbar.
	 * @return <code>true</code> on success, <code>false</code> otherwise.
	 */
	public boolean addToolbarElement(Node node) {
		if(toolbarElementsListener != null) {
			toolbarElementsListener.toolbarAdd(node); 
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Removes a formerly added Node from the global toolbar.
	 * @param node Node to remove from the global toolbar. 
	 * @return <code>true</code> on success, <code>false</code> otherwise.
	 */
	public boolean removeToolbarElement(Node node) {
		if(toolbarElementsListener != null) {
			toolbarElementsListener.toolbarRemove(node);
			return true;
		} else {
			return false;
		}
	}
	
	
	/**
	 * Sets the listener which is informed about changes in the executed Launchable history. 
	 * See informHistoryListener().
	 * @param listener ContextHistoryListener 
	 */
	public void setContextHistoryListener(ContextHistoryListener listener) {
		this.historyListener = listener;
	}
	
	/**
	 * Sets the listener which is informed about the current Launchable's name
	 * @param listener
	 */
	public void setLaunchableNamingListener(LaunchableNamingListener listener) {
		this.launchableNamingListener = listener;
	}
	
	/**
	 * Sets the listener which is informed about elements which want to enter the 
	 * global toolbar.
	 * @param listener ToolbarElementsListener
	 */
	public void setToolbarElementsListener(ToolbarElementsListener listener) {
		this.toolbarElementsListener = listener;
	}
	
	/**
	 * Sets the listener which is informed about modal message overlays.
	 * @param listener ModalMessageListener
	 */
	public void setModalMessageListener(ModalMessageListener listener) {
		this.modalMessageListener = listener;
	}
	
	
	/**
	 * @return String The name of the currently shown Launchable app.
	 */
	public String getCurrentLaunchableName() {
		return history.peek().getAppName();
	}
	
	/**
	 * @return Image The symbol/icon of the currently shown Launchable app.
	 */
	public Image getCurrentLaunchableImage() {
		return history.peek().getAppImage();
	}
}
