package de.tu_darmstadt.informatik.tk.flexdisp.launchable.utils;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

/**
 * Class to schedule tasks (<code>Runnable</code> objects). They can be started with delay and be executed repeatedly with 
 * fixed delay between consecutive executions.
 */
public class SchedulerUtils {

	/**
	 * Continuously runs a <code>Runnable</code> with fixed delay and given initial delay.
	 * @param initialDelay Delay before first execution of given <code>Runnable</code>.
	 * @param delay Fixed delay between consecutive executions of given <code>Runnable</code>.
	 * @param runnable The <code>Runnable</code> object to schedule.
	 * @return <code>ScheduledFuture</code> object for the continuous execution. This can be used to control the execution, e.g. abort it.
	 */
	public static ScheduledFuture<?> schedule(int initialDelay, int delay, Runnable runnable) {
		ScheduledExecutorService scheduledExecutorService = Executors.newSingleThreadScheduledExecutor(new ThreadFactory() {
	        @Override
	        public Thread newThread(Runnable r) {
	            Thread thread = new Thread(r);
	            thread.setDaemon(true);
	            return thread;
	        }
	    });
		
		ScheduledFuture<?> future = scheduledExecutorService.scheduleWithFixedDelay(runnable, initialDelay, delay, TimeUnit.MILLISECONDS);
		return future;
	}
	
	/**
	 * 
	 * @param initialDelay Delay before first execution of given <code>Runnable</code>.
	 * @param runnable The <code>Runnable</code> object to schedule.
	 * @return <code>ScheduledFuture</code> object for the continuous execution. This can be used to control the execution, e.g. abort it.
	 */
	public static ScheduledFuture<?> scheduleRunOnce(int initialDelay, Runnable runnable) {
		ScheduledExecutorService scheduledExecutorService = Executors.newSingleThreadScheduledExecutor(new ThreadFactory() {
	        @Override
	        public Thread newThread(Runnable r) {
	            Thread thread = new Thread(r);
	            thread.setDaemon(true);
	            return thread;
	        }
	    });
		
		ScheduledFuture<?> future = scheduledExecutorService.schedule(runnable, initialDelay, TimeUnit.MILLISECONDS);
		return future;
	}
	
}
