package de.tu_darmstadt.informatik.tk.flexdisp.launchable.listener;

/**
 * Listener, to inform about modal overlay messages
 * @author Michael Winkler
 *
 */
public interface ModalMessageListener {

	/**
	 * Called, when a modal message enters the scene.
	 */
	public void modalMessageEntered();
	
	/**
	 * Classed, when a modal message leaves the scene. 
	 */
	public void modalMessageQuitted();
	
}
