package de.tu_darmstadt.informatik.tk.flexdisp.launchable.listener;

/**
 * Listener, used by Context, to inform the global menu bar.
 * @author Michael Winkler
 *
 */
public interface ContextHistoryListener {
	/**
	 * Called, when there is a applet history and the global back button
	 * should be enabled.
	 */
	public abstract void historyAvailable();
	
	/**
	 * Called, when there is no applet history and the global back button
	 * should be disabled.
	 */
	public abstract void historyEmpty();
}
