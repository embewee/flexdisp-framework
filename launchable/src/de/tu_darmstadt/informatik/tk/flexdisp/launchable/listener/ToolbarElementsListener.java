package de.tu_darmstadt.informatik.tk.flexdisp.launchable.listener;

import javafx.scene.Node;

/**
 * Listener used to add and remove application specific elements
 * to / from the global menu bar. 
 * @author Michael Winkler
 *
 */
public interface ToolbarElementsListener {
	/**
	 * Called, when a node is added to the menu bar
	 * @param node
	 */
	public void toolbarAdd(Node node);
	
	/**
	 * Called, when a node is removed from the menu bar
	 * @param node
	 */
	public void toolbarRemove(Node node);
	
	/**
	 * Called, when all application specific elements
	 * shall be removed from the menu bar
	 */
	public void toolbarClear();
}
