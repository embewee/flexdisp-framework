package de.tu_darmstadt.informatik.tk.flexdisp.launchable;

import javafx.scene.image.Image;
import javafx.scene.layout.Pane;

/**
 * Interface for applets. Applets that shall be executed in this 
 * framework, need to implement this interface.
 * @author Michael Winkler
 *
 */
public interface Launchable {
	/**
	 * Executed, when he applet enters the stage of the framework application and 
	 * becomes visible. <br>
	 * Register listeners and modify context here.
	 * @param context The application context
	 */
	public abstract void enterStage(Context context);
	
	/**
	 * Executed, when the applet is removed from the framework application.<br>
	 * Unregister listeners and revert the context here. 
	 */
	public abstract void quitStage();
	
	/**
	 * Returns the applet's image/icon/symbol, displayed in the
	 * right corner of the menu bar. 
	 * @return Image
	 */
	public abstract Image getAppImage();
	
	/**
	 * Returns the applet's title, which will be displayed in the
	 * menu bar.
	 * @return String
	 */
	public abstract String getAppName();
	
	/**
	 * Returns the applet's pane. This will be added to the scene.
	 * All elements to be printed on the screen must be a child of this pane.
	 * @return Pane
	 */
	public abstract Pane getAppPane();
}
