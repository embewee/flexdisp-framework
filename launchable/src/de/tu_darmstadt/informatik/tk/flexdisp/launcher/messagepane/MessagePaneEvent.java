package de.tu_darmstadt.informatik.tk.flexdisp.launcher.messagepane;

/**
 * Arbitrary event for message panes.
 * @author Michael Winkler
 *
 */
public interface MessagePaneEvent {
	public void executeEvent();
}
