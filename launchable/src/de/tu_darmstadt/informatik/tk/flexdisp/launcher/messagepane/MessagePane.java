package de.tu_darmstadt.informatik.tk.flexdisp.launcher.messagepane;

import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import de.tu_darmstadt.informatik.tk.flexdisp.launchable.Context;
import de.tu_darmstadt.informatik.tk.flexdisp.launchable.utils.FXUtils;

/**
 * Convenience class to show predefined overlays on the scene.
 * @author Michael Winkler
 *
 */
public class MessagePane extends Pane {
	
	private MessagePane() {}	
	
	
	/**
	 * Opens a non-modal overlay over the current app and shows the provided pane inside this overlay.
	 * The overlay contains a close button.
	 * @param context The Context
	 * @param pane Pane to show
	 */
	public static void overlay(Context context, Pane pane) {
		overlay(context, pane, true, null);
	}
	
	/**
	 * Opens a non-modal overlay over the current app and shows the provided pane inside this overlay.
	 * The overlay contains a close button.
	 * @param context The Context
	 * @param pane Pane to show
	 * @param showCloseButton if <code>true</code>, close button is visible, otherwise is won't be visible.
	 * @param event MessagePaneEvent to be executed before the overlay is closed. <b>Mind!:</b>
	 * Can only be triggered, if the close button is visible, so <code>showCloseButton</code> 
	 * must be true in order to be able to trigger this event.
	 */
	public static void overlay(Context context, Pane pane, boolean showCloseButton, MessagePaneEvent event) {
		HBox hbox = new HBox();
		
		StackPane parentPane = new StackPane();
		parentPane. setStyle("-fx-background-color: white;"
				+ "-fx-background-radius: 10 10 10 10;"
				+ "-fx-border-color: black;"
				+ "-fx-border-width: 2;"
				+ "-fx-border-radius: 10 10 10 10;"
				+ "-fx-padding: 20 20 20 20;"
				);
		
		int width = 800;
		int height = 400;
		
		parentPane.setMinSize(width, height);
		parentPane.setPrefSize(width, height);
		parentPane.setMaxSize(width, height);
		parentPane.setAlignment(Pos.CENTER);
		
		Region spacerLeft = new Region();
		HBox.setHgrow(spacerLeft, Priority.ALWAYS);
		spacerLeft.setMinWidth(Region.USE_PREF_SIZE);
		
		Region spacerRight = new Region();
		HBox.setHgrow(spacerRight, Priority.ALWAYS);
		spacerRight.setMinWidth(Region.USE_PREF_SIZE);
		
		if(showCloseButton) {
			Image closeImage = new Image(MessagePane.class.getResourceAsStream("close.png"));
			ImageView closeView = new ImageView(closeImage);
			closeView.setOnMouseClicked(e -> {
				if(event != null) { 
					event.executeEvent();
				}
				context.popMessage();
			});
			hbox.getChildren().addAll(spacerLeft, pane, spacerRight, closeView);
		} else {
			hbox.getChildren().addAll(spacerLeft, pane, spacerRight);
		}
		
		parentPane.getChildren().addAll(hbox);
		
		context.pushMessage(parentPane, true);
	}
	
	public static void info(Context context, String title, String msg) {
		try {
			final Pane p = FXMLLoader.load(MessagePane.class.getResource("MessagePane.fxml"));
			ImageView imageView = FXUtils.getChildByID(p, "messagePaneIcon");
			Label lblTitle = FXUtils.getChildByID(p, "messagePaneTitle");
			Label lblMessage = FXUtils.getChildByID(p, "messagePaneMessage");
			Button btnRight = FXUtils.getChildByID(p, "messagePaneButtonRight");

			Image icon = new Image(MessagePane.class.getResourceAsStream("information.png"));
			imageView.setImage(icon);
			lblTitle.setText(title);
			lblMessage.setText(msg);
			btnRight.setText("OK");
			btnRight.setVisible(true);
			btnRight.setOnAction(e -> {
				context.popMessage();
			});
			
			context.pushMessage(p, false);
		
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void warning(Context context, String title, String msg) {
		try {
			final Pane p = FXMLLoader.load(MessagePane.class.getResource("MessagePane.fxml"));
			ImageView imageView = FXUtils.getChildByID(p, "messagePaneIcon");
			Label lblTitle = FXUtils.getChildByID(p, "messagePaneTitle");
			Label lblMessage = FXUtils.getChildByID(p, "messagePaneMessage");
			Button btnRight = FXUtils.getChildByID(p, "messagePaneButtonRight");

			Image icon = new Image(MessagePane.class.getResourceAsStream("warning.png"));
			imageView.setImage(icon);
			lblTitle.setText(title);
			lblMessage.setText(msg);
			btnRight.setText("OK");
			btnRight.setVisible(true);
			btnRight.setOnAction(e -> {
				context.popMessage();
			});
			
			context.pushMessage(p, false);
		
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void error(Context context, String title, String msg) {
		try {
			final Pane p = FXMLLoader.load(MessagePane.class.getResource("MessagePane.fxml"));
			ImageView imageView = FXUtils.getChildByID(p, "messagePaneIcon");
			Label lblTitle = FXUtils.getChildByID(p, "messagePaneTitle");
			Label lblMessage = FXUtils.getChildByID(p, "messagePaneMessage");
			Button btnRight = FXUtils.getChildByID(p, "messagePaneButtonRight");

			Image icon = new Image(MessagePane.class.getResourceAsStream("error.png"));
			imageView.setImage(icon);
			lblTitle.setText(title);
			lblMessage.setText(msg);
			btnRight.setText("OK");
			btnRight.setVisible(true);
			btnRight.setOnAction(e -> {
				context.popMessage();
			});
			
			context.pushMessage(p, false);
		
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void fatalError(Context context, String title, String msg) {
		try {
			final Pane p = FXMLLoader.load(MessagePane.class.getResource("MessagePane.fxml"));
			ImageView imageView = FXUtils.getChildByID(p, "messagePaneIcon");
			Label lblTitle = FXUtils.getChildByID(p, "messagePaneTitle");
			Label lblMessage = FXUtils.getChildByID(p, "messagePaneMessage");
			
			Image icon = new Image(MessagePane.class.getResourceAsStream("error.png"));
			imageView.setImage(icon);
			lblTitle.setText(title);
			lblMessage.setText(msg);
			
			context.pushMessage(p, true);
		
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
