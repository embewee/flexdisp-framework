package de.tu_darmstadt.informatik.tk.flexdisp.videoapp;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * A button, that can show a Play-symbol and a Stop-symbol.
 * @author Michael Winkler
 *
 */
public class PlayButton extends ImageView {

	private Image imgPlay;
	private Image imgPause;
	
	public PlayButton() {
		imgPlay = new Image(getClass().getResourceAsStream("play.png"));
		imgPause = new Image(getClass().getResourceAsStream("pause.png"));
		setImage(imgPlay);
	}
	
	/**
	 * Shows the play image (>) on this button. 
	 */
	public void showPlaySymbol() {
		setImage(imgPlay);
	}
	
	/**
	 * Shows the pause image (||) on this button.
	 */
	public void showPauseSymbol() {
		setImage(imgPause);
	}
	
	
	
}
