package de.tu_darmstadt.informatik.tk.flexdisp.videoapp;

/**
 * Delivers the a speed rate for the player.
 * @author Michael Winkler
 *
 */
public class Speed {
	private double[] speed;
	private int index;
	
	private int RESET = 2; //1
	
	public Speed() {
		speed = new double[]{0, 0.5, 1, 2, 4};
		index = RESET;
	}
	
	public double nextSpeed() {
		int n = Math.min(index+1, speed.length -1);
		index = n;
		double s = speed[index];
		return s;		
	}
	
	public double prevSpeed() {
		int n = Math.max(index - 1, 0);
		index = n;
		double s = speed[index];
		return s;
	}
	
	public double currentSpeed() {
		return speed[index];
	}
	
	public void setToPause() {
		index = 0;
	}
	
	public void setToNormalSpeed() {
		index = RESET;
	}
}
