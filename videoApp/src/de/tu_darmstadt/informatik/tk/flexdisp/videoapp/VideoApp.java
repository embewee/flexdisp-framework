package de.tu_darmstadt.informatik.tk.flexdisp.videoapp;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.List;

import javafx.animation.FadeTransition;
import javafx.animation.ParallelTransition;
import javafx.animation.ScaleTransition;
import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.util.Duration;
import de.tu_darmstadt.informatik.tk.flexdisp.deformationsensor.sensor.DeformationEvent;
import de.tu_darmstadt.informatik.tk.flexdisp.deformationsensor.sensor.DeformationEventListener;
import de.tu_darmstadt.informatik.tk.flexdisp.launchable.Context;
import de.tu_darmstadt.informatik.tk.flexdisp.launchable.Launchable;
import de.tu_darmstadt.informatik.tk.flexdisp.launchable.utils.SchedulerUtils;

/**
 * The video applet's entry point. Displays the video selector.
 * @author Michael Winkler
 *
 */
public class VideoApp implements Launchable {
	
	private StackPane appPane;
	private Context context;
	
	private Pane currentImagePane;
	
	private VideoSelector videoSelector;
	private List<File> videoFiles;
	
	private int BLOCK_TIME = 500;
	
	private DeformationEventListener discreteListener;
	private DeformationEventListener continuousListener;
	
	private boolean selectFilmBlocked = false;
	private boolean scrollingBlocked = false;
	
	public VideoApp(File videoDirectory) {	
		appPane = new StackPane();
		appPane.setAlignment(Pos.CENTER);
		
		Image backgroundImage = new Image(getClass().getResourceAsStream("bg.jpg"));
		appPane.setBackground(new Background(new BackgroundImage(backgroundImage, 
				BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, BackgroundSize.DEFAULT)));
		
		Image filmStripeImage = new Image(getClass().getResourceAsStream("filmstripe.png"));
		videoFiles = findFilms(videoDirectory);
		videoSelector = new VideoSelector(videoFiles, filmStripeImage, 640 - 110, 480 - 64);
		Pane picturePane = videoSelector.getFrame();
		currentImagePane = picturePane;
		currentImagePane.setOnMouseClicked(e -> {
			filmSelected();
		});
		appPane.getChildren().add(picturePane);
		
		appPane.setOnScroll(event -> {
			int sign = (int) Math.signum(event.getDeltaY());
			if (sign > 0) {
				nextImage();
			} else if (sign < 0) {
				previousImage();
			}
		});
	}
	
	private void nextImage() {
		videoSelector.next();
		Pane comingImage = videoSelector.getFrame();
		swapBackground(currentImagePane, comingImage);
	}
	
	private void previousImage() {
		videoSelector.previous();
		Pane comingImage = videoSelector.getFrame();
		swapForeground(currentImagePane, comingImage);
	}
	
	/**
	 * Animate change
	 * @param oldImagePane
	 * @param comingImagePane
	 */
	private void swapBackground(Pane oldImagePane, Pane comingImagePane) {
		int duration = BLOCK_TIME;
		
		ScaleTransition st = new ScaleTransition(Duration.millis(duration), comingImagePane);
		st.setFromX(1.2);
		st.setFromY(1.2);
		st.setToX(1.0);
		st.setToY(1.0);

		FadeTransition ft = new FadeTransition(Duration.millis(duration), comingImagePane);
		ft.setFromValue(0.1);
		ft.setToValue(1.0);

		ParallelTransition p = new ParallelTransition(st, ft);

		appPane.getChildren().add(comingImagePane);

		p.play();
		
		p.setOnFinished(asdf -> {
			appPane.getChildren().remove(oldImagePane);
		});
		
		this.currentImagePane = comingImagePane;
		currentImagePane.setOnMouseClicked(e -> {
			filmSelected();
		});
	}
	
	/**
	 * Animate change
	 * @param oldImagePane
	 * @param comingImagePane
	 */
	private void swapForeground(Pane oldImagePane, Pane comingImagePane) {
		int duration = BLOCK_TIME;
		
		appPane.getChildren().add(comingImagePane);
		comingImagePane.toBack();		
		
		ScaleTransition st = new ScaleTransition(Duration.millis(duration), oldImagePane);
		st.setFromX(1.0);
		st.setFromY(1.0);
		st.setToX(1.2);
		st.setToY(1.2);

		FadeTransition ft = new FadeTransition(Duration.millis(duration), oldImagePane);
		ft.setFromValue(1.0);
		ft.setToValue(0.1);

		ParallelTransition p = new ParallelTransition(st, ft);

		p.play();
		
		p.setOnFinished(asdf -> {
			appPane.getChildren().remove(oldImagePane);
		});
		
		this.currentImagePane = comingImagePane;
		currentImagePane.setOnMouseClicked(e -> {
			filmSelected();
		});
	}
	
	/**
	 * @param directory Directory to look for films
	 * @return <code>List</code> of <code>File</code>s 
	 */
	private List<File> findFilms(File videoDirectory) {
		File[] files = new File[]{};
		files = videoDirectory.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				if(name.toLowerCase().endsWith(".flv")) {
					return true;
				} else if(name.toLowerCase().endsWith(".mp4")) {
					return true;
				} else {
					return false;
				}
			}
		});
		if(files == null || files.length == 0) {
			return new ArrayList<>();
		}
		
		return java.util.Arrays.asList(files);
	}
	
	@Override
	public void enterStage(Context context) {
		this.context = context;
		
		selectFilmBlocked = true;
		SchedulerUtils.scheduleRunOnce(1000, new Runnable() {
			@Override
			public void run() {
				selectFilmBlocked = false;
			}
		});
		
		block();
		
		discreteListener = new DeformationEventListener() {
			@Override
			public void newDeformationEvent(DeformationEvent deformationEvent) {
				switch (deformationEvent.getType()) {
				case UPPER_RIGHT_UP:
					if(!selectFilmBlocked) {
						filmSelected();
					}
					break;
				default:
					break;
				}
			}
		};
		context.getDeformationSensor().addDiscreteDeformationEventListener(discreteListener);

		continuousListener = new DeformationEventListener() {
			@Override
			public void newDeformationEvent(DeformationEvent deformationEvent) {
				switch (deformationEvent.getType()) {
				case MIDDLE_UP:
					scrollToFront();
					break;
				case MIDDLE_DOWN: 
					scrollToBack();
					break;
				default:
					break;
				}
			}
		};
		context.getDeformationSensor().addContinuousDeformationEventListener(continuousListener);
	}
	
	private void scrollToBack() {
		if(!scrollingBlocked) {
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					nextImage();
					block();
				}
			});			
		}
	}
	
	private void scrollToFront() {
		if(!scrollingBlocked) {
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					previousImage();
					block();
				}
			});			
		}
	}
	
	public synchronized void block() {
		scrollingBlocked = true;
		SchedulerUtils.scheduleRunOnce(1000, new Runnable() {
			@Override
			public void run() {
				scrollingBlocked = false;
			}
		});
	}
	
	@Override
	public void quitStage() {
		context.getDeformationSensor().removeContinuousDeformationEventListener(continuousListener);
		context.getDeformationSensor().removeDiscreteDeformationEventListener(discreteListener);
	}

	@Override
	public String getAppName() {
		return "Video";
	}

	@Override
	public Pane getAppPane() {
		return appPane;
	}

	@Override
	public Image getAppImage() {
		return new Image(getClass().getResourceAsStream("video.png"));
	}
	
	/**
	 * Opens the video view to play the video
	 */
	private void filmSelected() {
		VideoPlayer videoPlayer = new VideoPlayer(videoSelector.getFile());
		context.launch(videoPlayer);
	}
}
