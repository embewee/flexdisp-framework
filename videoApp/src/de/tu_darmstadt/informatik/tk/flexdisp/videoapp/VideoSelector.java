package de.tu_darmstadt.informatik.tk.flexdisp.videoapp;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FilenameUtils;

import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

/**
 * Convenient selector for videos.
 * @author Michael Winkler
 *
 */
public class VideoSelector {
	
	private List<File> files;
	private List<Image> images;
	
	private Image frameImage;
	private int currentIndex;
	private double fitWidth;
	private double fitHeight;

	/**
	 * 
	 * @param files List of video files. <b>MIND!</b> There must be a thumbnail for for each video, 
	 * named the same as the video, with extension .jpg, .jpeg or .png.  
	 * @param frameImage Image of the surrounding frame, the the video thumbnail is embedded in
	 * @param fitWidth Width to fit the thumbnail's size in
	 * @param fitHeight Height to fit the thumbnail's size in
	 */
	public VideoSelector(List<File> files, Image frameImage, double fitWidth, double fitHeight) {
		this.files = files;
		this.fitWidth = fitWidth;
		this.fitHeight = fitHeight;
		this.frameImage = frameImage;
		currentIndex = 0;
		
		images = findImages(files);
	}
	
	private List<Image> findImages(List<File> files) {
		ArrayList<Image> images = new ArrayList<>();
		
		for(File file : files) {
			Image image = getFilmThumbnail(file);
			images.add(image);
		}
		return images;
	}
	
	private Image getFilmThumbnail(File filmFile) {
		String fileName = filmFile.getAbsolutePath();
		
		String fileNameNoExt = FilenameUtils.removeExtension(fileName);
		
		String[] extensions = new String[]{".jpg", ".jpeg", ".png"};
		
		for(String extension : extensions) {
			File imageFile = new File(fileNameNoExt + extension);
			if(imageFile.exists()) {
				Image image = new Image(imageFile.toURI().toString());
				return image;
			}
		} 
		
		return null; 
	}
	
	/**
	 * Next video
	 */
	public void next() {
		timeMachineChangeCurrent(+1);
	}

	/**
	 * Previous video
	 */
	public void previous() {
		timeMachineChangeCurrent(-1);
	}

	private void timeMachineChangeCurrent(int direction) {
		if (!files.isEmpty()) {
			currentIndex = circleIndex(direction);
		}
	}
	
	private int circleIndex(int value) {
		return ((currentIndex + value) % files.size() 
				+ files.size()) % files.size();
	}

	/**
	 * 
	 * @return The thumbnail of the current film, embedded in the given frame.
	 */
	public Pane getFrame() {
		StackPane pane = new StackPane();
		ImageView frameView = new ImageView(frameImage);
		ImageView pictureView = new ImageView(images.get(currentIndex));
		pictureView.setFitWidth(fitWidth);
		pictureView.setFitHeight(fitHeight);
		
		Label lblFilmTitle = new Label(); 
		lblFilmTitle.setEffect(new DropShadow());
		lblFilmTitle.setAlignment(Pos.BOTTOM_CENTER);
		lblFilmTitle.setTextFill(Color.WHITE);
		lblFilmTitle.setFont(new Font(36));
		lblFilmTitle.setStyle("-fx-font-weight: bold;");
		lblFilmTitle.setTranslateY(180);
		String title = FilenameUtils.removeExtension(files.get(currentIndex).getName());
		lblFilmTitle.setText(title);
		
		pane.getChildren().addAll(frameView, pictureView, lblFilmTitle);
		return pane;
	}
	
	/**
	 * 
	 * @return the current video file.
	 */
	public File getFile() {
		return files.get(currentIndex);
	}
}

