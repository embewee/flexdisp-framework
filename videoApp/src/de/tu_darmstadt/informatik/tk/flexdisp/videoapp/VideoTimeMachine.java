package de.tu_darmstadt.informatik.tk.flexdisp.videoapp;

import java.io.File;
import java.util.List;

import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

import org.apache.commons.io.FilenameUtils;

public class VideoTimeMachine {
	
	private StackPane view;
	private ImageView imageView;
	private Label lblFilmTitle;
	private List<File> files;
	private int currentIndex;
	private Image currentVideoThumbnail;
	private double fitWidth;
	private double fitHeight;

	public VideoTimeMachine(List<File> filmFiles, double fitWidth, double fitHeight) {
		this.files = filmFiles;
		this.fitWidth = fitWidth;
		this.fitHeight = fitHeight;
		
		currentIndex = 0;
		
		view = new StackPane();
		view.setAlignment(Pos.CENTER);
		view.setOnScroll(event -> {
			int sign = (int) Math.signum(event.getDeltaY());
			if (sign > 0) {
				next();
			} else if (sign < 0) {
				previous();
			}
		});
		
		imageView = new ImageView();
		view.getChildren().add(imageView);
		
		lblFilmTitle = new Label(); 
		lblFilmTitle.setEffect(new DropShadow());
		lblFilmTitle.setAlignment(Pos.BOTTOM_CENTER);
		lblFilmTitle.setTextFill(Color.WHITE);
		lblFilmTitle.setFont(new Font(36));
		lblFilmTitle.setStyle("-fx-font-weight: bold;");
		lblFilmTitle.setTranslateY(180);
		view.getChildren().add(lblFilmTitle);
		
		setCurrent();
	}
	
	public StackPane getView() {
		return view;
	}
	
	public void next() {
		timeMachineChangeCurrent(+1);
	}

	public void previous() {
		timeMachineChangeCurrent(-1);
	}

	private void timeMachineChangeCurrent(int direction) {
		if (!files.isEmpty()) {
			int prev = ((currentIndex + direction) % files.size() 
					+ files.size()) % files.size();
			currentIndex = prev;
			setCurrent();
		}
	}
	
	private Image getFilmThumbnail(File filmFile) {
		String fileName = filmFile.getAbsolutePath();
		
		String fileNameNoExt = FilenameUtils.removeExtension(fileName);
		
		String[] extensions = new String[]{".jpg", ".jpeg", ".png"};
		
		for(String extension : extensions) {
			File imageFile = new File(fileNameNoExt + extension);
			if(imageFile.exists()) {
				Image image = new Image(imageFile.toURI().toString());
				return image;
			}
		} 
		
		return null; 
	}

	public void setCurrent() {
		if (!files.isEmpty()) {
			File file = files.get(currentIndex);
			currentVideoThumbnail = getFilmThumbnail(file);
			
			if(currentVideoThumbnail != null) {
				Platform.runLater(new Runnable() {
					@Override
					public void run() {
						imageView.setImage(currentVideoThumbnail);
						imageView.setFitWidth(fitWidth);
						imageView.setFitHeight(fitHeight);
						String title = FilenameUtils.removeExtension(file.getName());
						lblFilmTitle.setText(title);
					}
				});
			} else {
				System.err.println("COULD NOT FIND IMAGE");
			}
		}
	}

	public File getCurrentFile() {
		if (!files.isEmpty()) {
			return files.get(currentIndex);
		} else {
			return null;
		}
	}
	
	public int getCurrentIndex() {
		if (!files.isEmpty()) {
			return currentIndex;
		} else {
			return -1;
		}
	}
}

