package de.tu_darmstadt.informatik.tk.flexdisp.videoapp;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * A button, that displays an image.
 * @author Michael Winkler
 *
 */
public class ImageButton extends ImageView {
	private Image image;
	
	/**
	 * 
	 * @param image path to resource
	 */
	public ImageButton(String image) {
		this.image = new Image(getClass().getResourceAsStream(image));
		setImage(this.image);
	}
}
