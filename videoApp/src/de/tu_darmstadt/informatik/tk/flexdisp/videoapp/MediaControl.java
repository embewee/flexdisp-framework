package de.tu_darmstadt.informatik.tk.flexdisp.videoapp;

import java.io.File;

import javafx.application.Platform;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaPlayer.Status;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.util.Duration;

/**
 * Encapsulates the media controls.  
 * @author Based on code from Oracle's JavaFX exmaples: http://docs.oracle.com/javafx/2/media/playercontrol.htm<br>
 * adapted by Michael Winkler
 *
 */
public abstract class MediaControl extends VBox {
	private int FWD_RWND_VALUE = 10;

	private MediaPlayer mediaPlayer;
	private Duration videoRunningTime;
	
	private Slider timeSlider;
	private PlayButton btnPlayPause;
	private Label lblPlayTime;
	
	private Speed speed;
	
	private boolean wasPlaying = false;
	
	public MediaControl(MediaPlayer mediaPlayer, File videoFile) {
		this.mediaPlayer = mediaPlayer;
		layOutControl();
		speed = new Speed();
		configureMediaPlayer();
	}
	
	private void layOutControl() {
		setAlignment(Pos.CENTER);
		setPadding(new Insets(10, 10, 20, 10));
		setSpacing(40);
		BorderPane.setAlignment(this, Pos.BOTTOM_CENTER);
		setStyle("-fx-background-color: black");
		
		//container for play button, etc...
		ImageButton btnRwnd = new ImageButton("rwnd.png");
		btnRwnd.setOnMouseClicked(e -> {
			rewind();
		});
		
		btnPlayPause = new PlayButton();
		btnPlayPause.setOnMouseClicked(e -> {
			playPause();
		});
		
		ImageButton btnFwd = new ImageButton("fwd.png");
		btnFwd.setOnMouseClicked(e -> {
			forward();
		});

		//Box for buttons
		HBox hboxButtons = new HBox();
		hboxButtons.setSpacing(10);
		hboxButtons.setAlignment(Pos.CENTER);
		hboxButtons.getChildren().addAll(btnRwnd, btnPlayPause, btnFwd);
		getChildren().add(hboxButtons);

		
		
		//time slider
		timeSlider = new Slider();
		HBox.setHgrow(timeSlider, Priority.ALWAYS);
		timeSlider.setMaxWidth(Double.MAX_VALUE);
		
		timeSlider.valueProperty().addListener(new InvalidationListener() {
			public void invalidated(Observable ov) {
				if (timeSlider.isValueChanging()) {
					// multiply duration by percentage calculated by slider position
					Duration value = videoRunningTime.multiply(timeSlider.getValue() / 100.0);
					setPlayerToTime(value);
				} 
			}
		});
		
		//Video must be paused to drag the slider. Remember state before pausing.
		timeSlider.setOnMousePressed(e -> {
			if (mediaPlayer.getStatus() == Status.PLAYING) {
				wasPlaying = true;
				mediaPlayer.pause();
			}
		});
		
		timeSlider.setOnMouseClicked(e -> {
			Duration value = videoRunningTime.multiply(timeSlider.getValue() / 100.0);
			setPlayerToTime(value);
			if(wasPlaying) {
				mediaPlayer.play();
				wasPlaying = false;
			}
		});
		
		HBox velPane = new HBox();
		velPane.setSpacing(10);
		Button btnSlower = new Button("-");
		btnSlower.setOnMouseClicked(e -> {
			slower();
		});
		Button btnFaster = new Button("+");
		btnFaster.setOnMouseClicked(e -> {
			faster();
		});
		velPane.getChildren().addAll(btnSlower,btnFaster);
		
		// video speed label and play time label
		Region spacer = new Label("");
		spacer.setMinWidth(150);
		spacer.setPrefWidth(150);
		spacer.setMaxWidth(150);
		
		lblPlayTime = new Label();
		lblPlayTime.setFont(new Font(20));
		lblPlayTime.setMinWidth(150);
		lblPlayTime.setPrefWidth(150);
		lblPlayTime.setMaxWidth(150);
		lblPlayTime.setTextFill(Color.WHITE);
		
		// put time slider and labels together
		HBox hboxSlider = new HBox();
		hboxSlider.setSpacing(10);
		hboxSlider.setAlignment(Pos.CENTER);
		hboxSlider.getChildren().addAll(spacer, timeSlider, lblPlayTime, velPane);
		
		//put all together
		getChildren().add(hboxSlider);
	}

	private void configureMediaPlayer() {
		mediaPlayer.currentTimeProperty().addListener(new InvalidationListener() {
			public void invalidated(Observable ov) {
				updateValues();
			}
		});
		
		mediaPlayer.setOnPlaying(new Runnable() {
			public void run() {
				btnPlayPause.showPauseSymbol();
			}
		});

		mediaPlayer.setOnPaused(new Runnable() {
			public void run() {
				btnPlayPause.showPlaySymbol();
			}
		});

		mediaPlayer.setOnReady(new Runnable() {
			public void run() {
				videoRunningTime = mediaPlayer.getMedia().getDuration();
				updateValues();
			}
		});

		mediaPlayer.setOnEndOfMedia(new Runnable() {
			public void run() {
				btnPlayPause.showPlaySymbol();
				mediaPlayer.stop();
				mediaPlayer.seek(Duration.ZERO);
				mediaPlayer.pause();
			}
		});
		
//		mediaPlayer.setOnStopped(new Runnable() {
//			@Override
//			public void run() {
//				
//			}
//		});
//		
//		mediaPlayer.setOnHalted(new Runnable() {
//			@Override
//			public void run() {
//				
//			}
//		});
	}
	
	public void playPause() {
		switch(mediaPlayer.getStatus()) {
		case UNKNOWN:
		case HALTED:
			onError("Cannot play video", " Video status is UNKNOWN or HALTED.");
			System.err.println("Cannot play video: Status is UNKNOWN or HALTED.");
			onOverlayMessage("ERROR");
			break;
		case PAUSED:
		case READY:
		case STOPPED:
			mediaPlayer.play();
			mediaPlayer.setRate(1.0);
			speed.setToNormalSpeed();
			onOverlayMessage(">");
			break;
		case PLAYING:
			mediaPlayer.pause();
			speed.setToPause();
			onOverlayMessage("||");
			break;
		default:
			onOverlayMessage("�");
			
		}
	}
	
	public boolean isPlaying() {
		return mediaPlayer.getStatus() == MediaPlayer.Status.PLAYING;
	}
	
	private void setPlayerToTime(Duration duration) {
		mediaPlayer.seek(duration);
		updateValues();
	}
		
	public void forward() {
		mediaPlayer.seek(mediaPlayer.getCurrentTime().add(Duration.seconds(FWD_RWND_VALUE)));
		if (mediaPlayer.getStatus() != MediaPlayer.Status.PLAYING) {
			updateValues();
		}
		onOverlayMessage(">>");
	}
	
	public void rewind() {
		mediaPlayer.seek(mediaPlayer.getCurrentTime().subtract(Duration.seconds(FWD_RWND_VALUE)));
		if (mediaPlayer.getStatus() != MediaPlayer.Status.PLAYING) {
			updateValues();
		}
		onOverlayMessage("<<");
	}
	
	public void faster() {
		if(!isPlaying()) {
			playPause();
			onOverlayMessage(">");
		} else {
			String ret = setRate(speed.nextSpeed());
			onOverlayMessage(ret);
		}
	}
	
	public void slower() {
		if(!isPlaying()) {
			onOverlayMessage("||");
			return;
		}
		
		double s = speed.prevSpeed();
		if(s == 0) {
			playPause();
			onOverlayMessage("||");
		} else {
			String ret = setRate(s);
			onOverlayMessage(ret);
		}
	}
	
	private String setRate(double rate) {
		mediaPlayer.setRate(rate);
		String ret = Double.toString(rate) + "x";
		
		if(!isPlaying()) {
			playPause();
		}
		
		return ret;
	}
	
	/**
	 * Update video time at label and time slider position
	 */
	private void updateValues() {
		Platform.runLater(new Runnable() {
			public void run() {
				Duration currentTime = mediaPlayer.getCurrentTime();
				lblPlayTime.setText(formatTime(currentTime, videoRunningTime));
				if (!timeSlider.isDisabled()
						&& videoRunningTime.greaterThan(Duration.ZERO)
						&& !timeSlider.isValueChanging())
				{
					double value = currentTime.divide(videoRunningTime.toMillis()).toMillis() * 100;
					timeSlider.setValue(value);
				}
			}
		});
	}
	
	private static String formatTime(Duration elapsed, Duration duration) {
		int intElapsed = (int) Math.floor(elapsed.toSeconds());
		int elapsedHours = intElapsed / (60 * 60);
		if (elapsedHours > 0) {
			intElapsed -= elapsedHours * 60 * 60;
		}
		int elapsedMinutes = intElapsed / 60;
		int elapsedSeconds = intElapsed - elapsedHours * 60 * 60
				- elapsedMinutes * 60;

		if (duration.greaterThan(Duration.ZERO)) {
			int intDuration = (int) Math.floor(duration.toSeconds());
			int durationHours = intDuration / (60 * 60);
			if (durationHours > 0) {
				intDuration -= durationHours * 60 * 60;
			}
			int durationMinutes = intDuration / 60;
			int durationSeconds = intDuration - durationHours * 60 * 60
					- durationMinutes * 60;
			if (durationHours > 0) {
				return String.format("%d:%02d:%02d/%d:%02d:%02d", elapsedHours,
						elapsedMinutes, elapsedSeconds, durationHours,
						durationMinutes, durationSeconds);
			} else {
				return String.format("%02d:%02d/%02d:%02d", elapsedMinutes,
						elapsedSeconds, durationMinutes, durationSeconds);
			}
		} else {
			if (elapsedHours > 0) {
				return String.format("%d:%02d:%02d", elapsedHours,
						elapsedMinutes, elapsedSeconds);
			} else {
				return String.format("%02d:%02d", elapsedMinutes,
						elapsedSeconds);
			}
		}
	}
	
	/**
	 * Message to be shown on case of errors.
	 * @param title
	 * @param message
	 */
	public abstract void onError(String title, String message);
	
	/**
	 * Overlay to be shown on top if the video.
	 * @param overlayMessage
	 */
	public abstract void onOverlayMessage(String overlayMessage);
}