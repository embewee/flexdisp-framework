package de.tu_darmstadt.informatik.tk.flexdisp.videoapp;

import java.io.File;

import javafx.animation.FadeTransition;
import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.util.Duration;
import de.tu_darmstadt.informatik.tk.flexdisp.launchable.Context;
import de.tu_darmstadt.informatik.tk.flexdisp.launcher.messagepane.MessagePane;

/**
 * The video player Pane, which embeds the video and the control pane.
 * @author Based on code from Oracle's JavaFX exmaples: http://docs.oracle.com/javafx/2/media/playercontrol.htm<br>
 * Adapted by Michael Winkler
 *
 */
public class Player extends BorderPane {
	private Media media;
	private MediaPlayer mediaPlayer;
	private MediaView mediaView;
	private MediaControl mediaControl;
	private Label lblOverlay;
	
	public Player(Context context, File videoFile) {
		setStyle("-fx-background-color: black");
		
		media = new Media(videoFile.toURI().toString());
		mediaPlayer = new MediaPlayer(media);
		mediaView = new MediaView(mediaPlayer);
		
		mediaControl = new MediaControl(mediaPlayer, videoFile) {
			@Override
			public void onError(String title, String message) {
				MessagePane.error(context, title, message);
			}

			@Override
			public void onOverlayMessage(String overlayMessage) {
				overlay(overlayMessage);
			}
			
		};
		
		lblOverlay = new Label(); 
		lblOverlay.setEffect(new DropShadow());
		lblOverlay.setAlignment(Pos.BOTTOM_CENTER);
		lblOverlay.setTextFill(Color.WHITE);
		lblOverlay.setFont(new Font(90));
		lblOverlay.setStyle("-fx-font-weight: bold;");
		
		StackPane mediaViewPane = new StackPane();
		mediaViewPane.setAlignment(Pos.CENTER);
		mediaViewPane.getChildren().addAll(mediaView, lblOverlay);
		setCenter(mediaViewPane);

		setBottom(mediaControl);
	}
	
	public void faster() {
		mediaControl.faster();
	}
	
	public void slower() {
		mediaControl.slower();
	}

	public void playPause() {
		mediaControl.playPause();
	}
	
	public void rewind() {
		mediaControl.rewind();
	}
	
	public void forward() {
		mediaControl.forward();
	}
	
	private void overlay(String text) {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				lblOverlay.setText(text);
				FadeTransition ft = new FadeTransition(Duration.millis(2000), lblOverlay);
				ft.setFromValue(1.0);
				ft.setToValue(0);
				ft.play();
			}
		});		
	}
}
