package de.tu_darmstadt.informatik.tk.flexdisp.videoapp;

import java.io.File;

import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import de.tu_darmstadt.informatik.tk.flexdisp.deformationsensor.sensor.DeformationEvent;
import de.tu_darmstadt.informatik.tk.flexdisp.deformationsensor.sensor.DeformationEventListener;
import de.tu_darmstadt.informatik.tk.flexdisp.launchable.Context;
import de.tu_darmstadt.informatik.tk.flexdisp.launchable.Launchable;
import de.tu_darmstadt.informatik.tk.flexdisp.launchable.utils.SchedulerUtils;
import de.tu_darmstadt.informatik.tk.flexkit.jconnector.OperationMode;
import de.tu_darmstadt.informatik.tk.flexkit.jconnector.StageSnapshoter;

/**
 * The video player applet, part of the video application.<br>
 * In this applet, the video is displayed and functions to control the video
 * are offered. 
 * 
 *  
 * @author Michael Winkler
 *
 */
public class VideoPlayer implements Launchable {
	private Context context;
	
	private int BLOCK_TIME = 1000;
	
	private DeformationEventListener discreteListener;
	private DeformationEventListener continuousListener;
	
	private Player player;
	
	private boolean blocked = false;
	
	private int oldPartial;
	private int oldFull;
	private int oldPeriod;
	private int[][]  oldDitherMatrix;
	private OperationMode oldOperationMode;
	
	public VideoPlayer(File videoFile) {
		player = new Player(context, videoFile);
	}
	
	private void forward() {
		if(!blocked) {
			player.forward();
			block();
		}
	}
	
	private void rewind() {
		if(!blocked) {
			player.rewind();
			block();
		}
	}
	
	private void faster() {
		if(!blocked) {
			player.faster();
			block();
		}
	}
	
	private void slower() {
		if(!blocked) {
			player.slower();
			block();
		}
	}
	
	private void playPause() {
		player.playPause();
	}
	
	private synchronized void block() {
		blocked = true;
		SchedulerUtils.scheduleRunOnce(BLOCK_TIME, new Runnable() {
			@Override
			public void run() {
				blocked = false; //unblock after amount of time
			}
		});
	}
	
	@Override
	public void enterStage(Context context) {
		this.context = context;
		
		StageSnapshoter snapshoter = context.getSnapshoter();
		oldPartial = snapshoter.getContinuousUpdateRefreshType();
		oldFull = snapshoter.getContinuousFullUpdateRefreshType();
		oldPeriod = snapshoter.getPeriod();
		oldOperationMode = snapshoter.getOperationMode();
		oldDitherMatrix = snapshoter.getDitherMatrix();
		
		snapshoter.setPeriod(250);
		snapshoter.setRefreshTypesForContinuousUpdates(1, oldFull); //gute typen: 1, 3
		
		context.addCSS(getClass().getResource("video.css").toExternalForm());
		
		block();
		
		continuousListener = new DeformationEventListener() {
			@Override
			public void newDeformationEvent(DeformationEvent deformationEvent) {
				switch (deformationEvent.getType()) {
				case MIDDLE_DOWN:
					slower();
					break;
				case MIDDLE_UP:
					faster();
					break;
				case EDGE_RIGHT_DOWN:
					forward();
					break;
				case EDGE_LEFT_DOWN:
					rewind();
					break;
				default:
					break;
				}
			}
		};
		context.getDeformationSensor().addContinuousDeformationEventListener(continuousListener);
		
		discreteListener = new DeformationEventListener() {
			@Override
			public void newDeformationEvent(DeformationEvent deformationEvent) {
				switch (deformationEvent.getType()) {
//				case UPPER_RIGHT_DOWN:
				case UPPER_RIGHT_UP:
					playPause();
					break;
				default:
					break;
				}
				
			}
		};
		context.getDeformationSensor().addDiscreteDeformationEventListener(discreteListener);
	}
	
	@Override
	public void quitStage() {
		context.getDeformationSensor().removeContinuousDeformationEventListener(continuousListener);
		context.getDeformationSensor().removeDiscreteDeformationEventListener(discreteListener);
		context.removeCSS(getClass().getResource("video.css").toExternalForm());
		
		StageSnapshoter snapshoter = context.getSnapshoter();
		snapshoter.setDitherMatrix(oldDitherMatrix);
		snapshoter.setOperationMode(oldOperationMode);
		snapshoter.setRefreshTypesForContinuousUpdates(oldPartial, oldFull);
		snapshoter.setPeriod(oldPeriod);
		snapshoter.requestFullUpdate();
	}

	@Override
	public String getAppName() {
		return "";
	}

	@Override
	public Pane getAppPane() {
		return player;
	}

	@Override
	public Image getAppImage() {
		return null;
	}
}
