package de.tu_darmstadt.informatik.tk.flexdisp.launcher;

import java.io.IOException;

import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import de.tu_darmstadt.informatik.tk.flexdisp.launchable.Context;
import de.tu_darmstadt.informatik.tk.flexdisp.launchable.listener.ContextHistoryListener;
import de.tu_darmstadt.informatik.tk.flexdisp.launchable.listener.LaunchableNamingListener;
import de.tu_darmstadt.informatik.tk.flexdisp.launchable.listener.ModalMessageListener;
import de.tu_darmstadt.informatik.tk.flexdisp.launchable.listener.ToolbarElementsListener;
import de.tu_darmstadt.informatik.tk.flexdisp.launchable.utils.FXUtils;
/**
 * The global menu bar, displayed in the framework application. 
 * @author Michael Winkler
 *
 */
public class MenuBar extends AnchorPane {

	private BackButton globalBackButton;
	private Label globalTitleLabel;
	private Pane globalToolsPane;
	private ImageView globalIconView;
	
	/**
	 * 
	 * @param context A reference to the application context.
	 * @throws IOException if MenuBar.fxml cannot be found or read.
	 */
	public MenuBar(Context context) throws IOException {
		HBox menuBarPane = (HBox) FXMLLoader.load(getClass().getResource("MenuBar.fxml"));
		getChildren().add(menuBarPane);
		
		menuBarPane.setMaxHeight(80);
		AnchorPane.setTopAnchor(menuBarPane, 0.0);
		AnchorPane.setRightAnchor(menuBarPane, 0.0);
		AnchorPane.setBottomAnchor(menuBarPane, 0.0);
		AnchorPane.setLeftAnchor(menuBarPane, 0.0);		
		
		Pane globalBack = (Pane) FXUtils.getChildByID(menuBarPane, "globalBack");
		globalIconView = (ImageView) FXUtils.getChildByID(menuBarPane, "globalIcon");
		globalTitleLabel = (Label) FXUtils.getChildByID(menuBarPane, "globalTitle");
		globalToolsPane = (Pane) FXUtils.getChildByID(menuBarPane, "globalTools");
		
		globalBackButton = new BackButton();
		globalBack.getChildren().add(globalBackButton);
		globalBackButton.setOnMouseClicked(e -> {
			if(globalBackButton.isEnabled()) {
				context.back();
			}
		});
		
		context.setContextHistoryListener(new ContextHistoryListener() {
			@Override
			public void historyEmpty() {
				setBackDisabled();
			}
			
			@Override
			public void historyAvailable() {
				setBackActive();
			}
		});
		
		context.setLaunchableNamingListener(new LaunchableNamingListener() {
			@Override
			public void launchableNameChanged(String launchableName) {
				setMenuBarTitle(launchableName);
			}
			
			@Override
			public void launchableIconChanged(Image launchableImage) {
				setMenuBarIcon(launchableImage);
			}
		});
		
		context.setToolbarElementsListener(new ToolbarElementsListener() {
			@Override
			public void toolbarRemove(Node node) {
				removeToolbarElement(node);
			}
			
			@Override
			public void toolbarClear() {
				clearToolbar();
			}
			
			@Override
			public void toolbarAdd(Node node) {
				addToolbarElement(node);
			}
		});
		
		context.setModalMessageListener(new ModalMessageListener() {
			@Override
			public void modalMessageQuitted() {
				setBackActive();
			}
			
			@Override
			public void modalMessageEntered() {
				setBackDisabled();
			}
		});
	}
	
	/**
	 * Sets the application title, displayed in the menu bar. 
	 * @param title String
	 */
	public void setMenuBarTitle(String title) {
		globalTitleLabel.setText(title);
	}
	
	/**
	 * Sets the image, displayed in the right corner of the menu bar.
	 * @param image Image
	 */
	public void setMenuBarIcon(Image image) {
		globalIconView.setImage(image);
	}
	
	/**
	 * Disables the global back button.
	 */
	public void setBackDisabled() {
		globalBackButton.disable();
	}
	
	/**
	 * Enables the global back button.
	 */
	public void setBackActive() {
		globalBackButton.enable();
	}
	
	/**
	 * Adds a application specific element to the global menu bar. 
	 * @param node Node
	 */
	public void addToolbarElement(Node node) {
		globalToolsPane.getChildren().add(node);
	}
	
	/**
	 * Removes a formerly added, application specific element from the menu bar.
	 * @param node Node
	 */
	public void removeToolbarElement(Node node) {
		globalToolsPane.getChildren().remove(node);
	}
	
	/**
	 * Removes all application specific elements from the menu bar. 
	 */
	public void clearToolbar() {
		globalToolsPane.getChildren().clear();
	}
}
