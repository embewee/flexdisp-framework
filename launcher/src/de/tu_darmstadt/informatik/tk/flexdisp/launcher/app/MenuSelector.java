package de.tu_darmstadt.informatik.tk.flexdisp.launcher.app;

import java.util.ArrayList;

import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.scene.image.Image;

/**
 * Application selector used by the framework application's application menu.  
 * @author Michael Winkler
 *
 */
public abstract class MenuSelector {
	private ArrayList<Card> cards;
	private int current;

	public MenuSelector() {
		cards = new ArrayList<>();
		current = 0;
	}

	public void addEntry(String name, String description,
			Image image, EventHandler<?> eventHandler) {
		Card photoApp = new Card(name, description, image, eventHandler);
		cards.add(photoApp);
		if(cards.size() > 0) {
			next();
		}
	}

	public void next() {
		int left = current;
		int cur = calculateRingIndex(+1);
		int right = calculateRingIndex(+2);
		updateInternal(left, cur, right);
		current = cur;
	}

	public void previous() {
		int left = calculateRingIndex(-2);
		int cur = calculateRingIndex(-1);
		int right = current;
		updateInternal(left, cur, right);
		current = cur;
	}

	private int calculateRingIndex(int direction) {
		if (!cards.isEmpty()) {
			return ((current + direction) % cards.size() + cards.size()) % cards.size();
		} else {
			return -1;
		}
		
	}

	private void updateInternal(int left, int cur, int right) {
		if (!cards.isEmpty()) {
			
			Card leftCard = cards.get(left); 
			Card currentCard = cards.get(cur);
			Card rightCard = cards.get(right); 
			
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					update(leftCard.getImage(), leftCard.getTitle(), leftCard.getDescription(), 
							currentCard.getImage(), currentCard.getTitle(), currentCard.getDescription(), 
							rightCard.getImage(), rightCard.getTitle(), rightCard.getDescription());
				}
			});
		}
	}
	
	public abstract void update(Image leftImage, String leftTitle, String leftDescription,
			Image currentImage, String currentTitle, String currentDescription,
			Image rightImage, String rightTitle, String rightDescription);

	public void executeCurrent() {
		if (!cards.isEmpty()) {
			Card card = cards.get(current);
			card.invoke();
		}
	}
}

/**
 * The application menu shows a card for each entry. The card's entries
 * are encapsulated in this class. 
 * @author Michael Winkler
 */
class Card {

	private String title;
	private String description;
	private Image image;
	private EventHandler<?> eventHandler;
	
	public Card(String title, String description, Image image, EventHandler<?> e) {
		this.title = title;
		this.description = description;
		this.image = image;
		this.eventHandler = e;
	}

	public String getTitle() {
		return title;
	}

	public String getDescription() {
		return description;
	}

	public Image getImage() {
		return image;
	}
	
	/**
	 * Runs the given EventHandler
	 */
	public void invoke() {
		eventHandler.handle(null);
	}
}