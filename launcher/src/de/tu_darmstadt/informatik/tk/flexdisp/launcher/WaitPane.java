package de.tu_darmstadt.informatik.tk.flexdisp.launcher;

import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

/**
 * Overlay pane to visualize pending operation. Progress can be set.
 * @author Michael Bjoern Winkler
 */
public class WaitPane extends VBox {

	private Pane parent;
	private ProgressIndicator progressIndicator;
	
	/**
	 * 
	 * @param parent Parent pane to add this pane to.
	 * @param showImmediately boolean. If <code>true</code>, the pane is immediately added to the parent pane.
	 * Otherwise <code>show()</code> must be called to make this pane visible.
	 */
	public WaitPane(Pane parent, boolean showImmediately) {
		this.parent = parent;
		setAlignment(Pos.CENTER);
		setSpacing(30.0);
		
		Label lblInfo = new Label("Calibrating bend sensor...");
		lblInfo.setFont(new Font(22.0));
		
		setStyle("-fx-background-color: white;"
				+ "-fx-background-radius: 10 10 10 10;"
				+ "-fx-border-color: black;"
				+ "-fx-border-width: 2;"
				+ "-fx-border-radius: 10 10 10 10;"
				);
		
		int prefWidth = 600;
		int prefHeight = 320;
		
		setMinSize(prefWidth, prefHeight);
		setPrefSize(prefWidth, prefHeight);
		setMaxSize(prefWidth, prefHeight);
		setLayoutX((1280 - prefWidth) / 2);
		setLayoutY((960 - prefHeight) / 2);
		
		progressIndicator = new ProgressIndicator();
		progressIndicator.setProgress(-1);
		progressIndicator.setPrefSize(150, 150);
		
		getChildren().addAll(lblInfo, progressIndicator);
		
		if(showImmediately) {
			show();
		}
	}
	
	/**
	 * Sets the progress of the progress indicator. Set to <code>-1</code> for indeterminate.  
	 * @param value
	 */
	public void setProgress(double value) {
		progressIndicator.setProgress(value);
	}
	
	/**
	 * Adds this pane to the given parent's children. Sets the progress indicator to indeterminate. 
	 */
	public void show() {
		progressIndicator.setProgress(-1);
		parent.getChildren().add(this);
	}
	
	/**
	 * Removes this pane from the given parent's children.
	 */
	public void hide() {
		parent.getChildren().remove(this);
	}
	
}
