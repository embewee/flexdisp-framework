package de.tu_darmstadt.informatik.tk.flexdisp.launcher;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * Global back button, displayed in the framework's global menu bar.
 * @author Michael Winkler
 *
 */
public class BackButton extends ImageView {

	private Image imgDisabled;
	private Image imgActive;
	private boolean enabled;
	
	/**
	 * Creates the global back button.
	 */
	public BackButton() {
		imgDisabled = new Image(Main.class.getResourceAsStream("backEmpty.png"));
		imgActive = new Image(Main.class.getResourceAsStream("backArrow.png"));
		setImage(imgDisabled);
		enabled = false;
	}
	
	/**
	 * Disables the global back button.
	 */
	public void disable() {
		setImage(imgDisabled);
		enabled = false;
	}
	
	/**
	 * Enables the global back button.
	 */
	public void enable() {
		setImage(imgActive);
		enabled = true;
	}
	
	/**
	 * Returns the state of the gloal back button.
	 * @return true, global back button is enabled, false otherwise.
	 */
	public boolean isEnabled() {
		return enabled;
	}
	
	
	
}
