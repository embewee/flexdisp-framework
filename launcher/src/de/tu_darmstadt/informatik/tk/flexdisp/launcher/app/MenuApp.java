package de.tu_darmstadt.informatik.tk.flexdisp.launcher.app;

import java.util.concurrent.Future;

import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.effect.PerspectiveTransform;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import de.tu_darmstadt.informatik.tk.flexdisp.deformationsensor.sensor.DeformationEvent;
import de.tu_darmstadt.informatik.tk.flexdisp.deformationsensor.sensor.DeformationEventListener;
import de.tu_darmstadt.informatik.tk.flexdisp.launchable.Context;
import de.tu_darmstadt.informatik.tk.flexdisp.launchable.Launchable;
import de.tu_darmstadt.informatik.tk.flexdisp.launchable.utils.FXUtils;
import de.tu_darmstadt.informatik.tk.flexdisp.launchable.utils.SchedulerUtils;

/**
 * The framework application's application menu
 * @author Michael Winkler
 *
 */
public class MenuApp implements Launchable {

	private StackPane menuPane;
	private Context context;
	private MenuSelector menuSelector;
	private DeformationEventListener continuousListener;
	
	private int BLOCK_TIME = 600;
	
	private boolean scrollingBlocked = false;
	
	private ImageView rightAppIcon;
	private Label rightAppTitle;
	private Label rightAppDescription;
	
	private ImageView currentAppIcon;
	private Label currentAppTitle;
	private Label currentAppDescription;
	
	private ImageView leftAppIcon;
	private Label leftAppTitle;
	private Label leftAppDescription;
	
	private String MENU_SELECTOR_FXML = "MenuSelector.fxml";

	public MenuApp() {
		menuSelector = new MenuSelector() {

			@Override
			public void update(Image leftImage, String leftTitle,
					String leftDescription, Image currentImage,
					String currentTitle, String currentDescription,
					Image rightImage, String rightTitle, String rightDescription) {

				leftAppIcon.setImage(leftImage);
				leftAppTitle.setText(leftTitle);
				leftAppDescription.setText(leftDescription);
				
				currentAppIcon.setImage(currentImage);
				currentAppTitle.setText(currentTitle);
				currentAppDescription.setText(currentDescription);
				
				rightAppIcon.setImage(rightImage);
				rightAppTitle.setText(rightTitle);
				rightAppDescription.setText(rightDescription);
			}
		};

		Pane currentAppPane = null;
		try {
			currentAppPane = (Pane) FXMLLoader.load(getClass().getResource(MENU_SELECTOR_FXML));
		} catch (Exception e) {
			e.printStackTrace();
			Platform.exit();
		}
		
		currentAppPane.setStyle(currentAppPane.getStyle() + "-fx-border-width: 7; -fx-border-color: black; -fx-background-color: white;");

		currentAppPane.setOnScroll(event -> {
			int signY = (int) Math.signum(event.getDeltaY());
			if (signY > 0) {
				menuSelector.next();
			} else if (signY < 0) {
				menuSelector.previous();
			}
			
			int signX = (int) Math.signum(event.getDeltaX());
			if (signX > 0) {
				menuSelector.next();
			} else if (signX < 0) {
				menuSelector.previous();
			}
		});
		currentAppPane.setOnMouseClicked(e -> {
			menuSelector.executeCurrent();
		});
		
		currentAppIcon = (ImageView) FXUtils.getChildByID(currentAppPane,"timeMachineIcon");
		currentAppTitle = (Label) FXUtils.getChildByID(currentAppPane,"timeMachineTitle");
		currentAppDescription = (Label) FXUtils.getChildByID(currentAppPane,"timeMachineDescription");
		
		Pane leftAppPane = null;
		try {
			leftAppPane = (Pane) FXMLLoader.load(getClass().getResource(MENU_SELECTOR_FXML));
		} catch (Exception e) {
			e.printStackTrace();
			Platform.exit();
		}

		leftAppIcon = (ImageView) FXUtils.getChildByID(leftAppPane,"timeMachineIcon");
		leftAppTitle = (Label) FXUtils.getChildByID(leftAppPane,"timeMachineTitle");
		leftAppDescription = (Label) FXUtils.getChildByID(leftAppPane,"timeMachineDescription");
		
		PerspectiveTransform pt1 = new PerspectiveTransform();
        pt1.setUlx(80);
        pt1.setUly(40);
        
        pt1.setUrx(400);
        pt1.setUry(10);
        
        pt1.setLrx(400);
        pt1.setLry(210);
        
        pt1.setLlx(80);
        pt1.setLly(180);
        leftAppPane.setEffect(pt1);
		
		Pane rightAppPane = null;
		try {
			rightAppPane = (Pane) FXMLLoader.load(getClass().getResource(MENU_SELECTOR_FXML));
		} catch (Exception e) {
			e.printStackTrace();
			Platform.exit();
		}
		
		rightAppIcon = (ImageView) FXUtils.getChildByID(rightAppPane,"timeMachineIcon");
		rightAppTitle = (Label) FXUtils.getChildByID(rightAppPane,"timeMachineTitle");
		rightAppDescription = (Label) FXUtils.getChildByID(rightAppPane,"timeMachineDescription");
		
		PerspectiveTransform pt2 = new PerspectiveTransform();
		pt2.setUlx(0);//
        pt2.setUly(10);
        
        pt2.setUrx(320);
        pt2.setUry(40);
        
        pt2.setLrx(320);
        pt2.setLry(180);
        
        pt2.setLlx(0);//
        pt2.setLly(210);
        rightAppPane.setEffect(pt2);
		
		
		HBox stripe = new HBox();
		stripe.setMinSize(1280, 220);
		stripe.setPrefSize(1280, 220);
		stripe.setMaxSize(1280, 220);
		Region spacer1 = new Region();
		HBox.setHgrow(spacer1, Priority.ALWAYS);
		Region spacer2 = new Region();
		HBox.setHgrow(spacer2, Priority.ALWAYS);
		stripe.getChildren().addAll(leftAppPane, spacer1, currentAppPane, spacer2, rightAppPane);
		
		menuPane = new StackPane();
		
		Image backgroundImage = new Image(getClass().getResourceAsStream("bg.jpg"));
		menuPane.setBackground(new Background(new BackgroundImage(backgroundImage, 
				BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, BackgroundSize.DEFAULT)));
		
		menuPane.setAlignment(Pos.CENTER);
		menuPane.getChildren().add(stripe);
		
		menuPane.setFocusTraversable(true);
		menuPane.setOnKeyPressed(e -> {
			if (e.getCode() == KeyCode.RIGHT) {
				menuSelector.previous();
			} else if (e.getCode() == KeyCode.LEFT) {
				menuSelector.next();
			} else if (e.getCode() == KeyCode.ENTER) {
				select();
			}
		});
	}
	
	public void addMenuEntry(String name, String description,
			Image image, EventHandler<?> eventHandler) {
		menuSelector.addEntry(name, description, image, eventHandler);
	}
	
	private void scrollUp() {
		if(!scrollingBlocked) {
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					menuSelector.next();
					block();
				}
			});			
		}
	}
	
	private void scrollDown() {
		if(!scrollingBlocked) {
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					menuSelector.previous();
					block();
				}
			});			
		}
	}
	
	private void select() {
		menuSelector.executeCurrent();
	}
	
	public synchronized void block() {
		scrollingBlocked = true;
		@SuppressWarnings("unused")
		Future<?> futureScrollingBlocked = SchedulerUtils.scheduleRunOnce(BLOCK_TIME, new Runnable() {
			@Override
			public void run() {
				scrollingBlocked = false;
			}
		});
	}
	
	@Override
	public void enterStage(Context context) {
		this.context = context;
		
		continuousListener = new DeformationEventListener() {
			@Override
				public void newDeformationEvent(DeformationEvent deformationEvent) {
					switch(deformationEvent.getType()) {
					case MIDDLE_DOWN:
					case MIDDLE_UP:
						select();
						break;
					case EDGE_RIGHT_DOWN:
						scrollDown();
						break;
					case EDGE_LEFT_DOWN:
						scrollUp();
						break;
					default:
						break;
					}
				}
			};
		
			block();
			
			context.getDeformationSensor().addContinuousDeformationEventListener(continuousListener);
	}
	
	@Override
	public void quitStage() {
		context.getDeformationSensor().removeContinuousDeformationEventListener(continuousListener);
	}

	@Override
	public String getAppName() {
		return "Men�";
	}

	@Override
	public Pane getAppPane() {
		return menuPane;
	}

	@Override
	public Image getAppImage() {
		Image appIcon = new Image(getClass().getResourceAsStream("menuicon.png"));
		return appIcon;
	}


}
