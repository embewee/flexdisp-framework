package de.tu_darmstadt.informatik.tk.flexdisp.launcher;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.InvalidPropertiesFormatException;
import java.util.Map;
import java.util.Properties;

import javafx.application.Application;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import de.tu_darmstadt.informatik.tk.flexdisp.deformationsensor.sensor.DeformationSensor;
import de.tu_darmstadt.informatik.tk.flexdisp.launchable.Context;
import de.tu_darmstadt.informatik.tk.flexdisp.launcher.app.MenuApp;
import de.tu_darmstadt.informatik.tk.flexdisp.launcher.messagepane.MessagePane;
import de.tu_darmstadt.informatik.tk.flexdisp.mapsapp.MapApp;
import de.tu_darmstadt.informatik.tk.flexdisp.photoapp.PhotoApp;
import de.tu_darmstadt.informatik.tk.flexdisp.videoapp.VideoApp;
import de.tu_darmstadt.informatik.tk.flexkit.jconnector.ConnectorException;
import de.tu_darmstadt.informatik.tk.flexkit.jconnector.MonochromeFlexKitConnector;
import de.tu_darmstadt.informatik.tk.flexkit.jconnector.OperationMode;
import de.tu_darmstadt.informatik.tk.flexkit.jconnector.StageSnapshoter;

/**
 * Entry point for the framework application.<br>
 * Reads the configuration file, starts the deformation sensor
 * and snapshoter, displays the appliction selection menu.  
 * @author Michael Winkler
 */
public class Main extends Application {
	private Context context;
	
	private DeformationSensor deformationSensor;
	private boolean startDeformationSensor = true;
	private int deformationSensorKValue;
	private int deformationSensorSmoothing;
	
	private StageSnapshoter snapshoter;
	private boolean startSnapshoter = false;
	private int snapshoterRefreshTypeForContinuousUpdatePartial = 2;
	private int snapshoterRefreshTypeForContinuousUpdateFull = 9;
	private int snapshoterRefreshPeriod = 1000;
	
	private String deformationSensorSerialPort;
	private File videoDirectory;
	private File photoDirectory;
	private File learnedDataSetFile;

	
	public static void main(String[] args) {
		launch(args);
	}
	
	@Override
	public void start(Stage stage) {
		BorderPane root = new BorderPane();
		
		Scene scene = new Scene(root, 1280, 960);
		scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		StackPane appStage = new StackPane();
		root.setCenter(appStage);
		
		//Init and show stage
		stage.setOnCloseRequest(e -> {
			stopSnapshoter();
			stopDeformationSensor();
		});
		stage.setScene(scene);
		stage.sizeToScene();
		stage.setResizable(false);
		stage.show();
		
		//create Context, set listeners for history to enable/disable global back button
		context = new Context(scene, appStage);

		try {
			initializeConfiguration();
		} catch (Exception e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
		//make global menu bar
		try {
			final MenuBar menuBar = new MenuBar(context);
			root.setTop(menuBar);
		} catch (IOException e1) {
			MessagePane.fatalError(context, "Could not create menu bar", e1.getMessage());
		}
	
		startDeformationSensor();
		startSnaphsoter(stage);
		initMenuApp();
	}

	/**
	 * Handles command line arguments.<br>
	 * Command line arguments override loaded configurations. 
	 */
	private void initializeConfiguration() throws Exception {
		Map<String, String> params = getParameters().getNamed();
		
		String configFile = params.get("config");
		
		System.out.println("Configuration file: " + configFile);
		
		Properties props = null;
		try {
			props = loadConfig(configFile);
		} catch (FileNotFoundException e) {
			MessagePane.warning(context, "No configuration file found", "The configuration file could not be found."
					+ "\n\nSpecify --config command line argument to use a configuration file.");			
		}
		
		if(props == null) {
			props = new Properties();
		}
		
		String deformationSensorParam = params.get("deformationsensor");
		if(deformationSensorParam != null && !deformationSensorParam.isEmpty()) {
			startDeformationSensor = Boolean.parseBoolean(deformationSensorParam);
			props.setProperty("deformationsensor", deformationSensorParam);
		}
		
		String snapshoterParam = params.get("snapshoter");
		if(snapshoterParam != null && !snapshoterParam.isEmpty()) {
			startSnapshoter = Boolean.parseBoolean(snapshoterParam);
			props.setProperty("snapshoter", snapshoterParam);
		}
		
		String portParam = params.get("port");
		if(portParam != null && !portParam.isEmpty()) {
			deformationSensorSerialPort = portParam;
			props.setProperty("port", portParam);
		}
		
		context.setProperties(props);
	}

	private void initMenuApp() {
		MenuApp app = new MenuApp();

		app.addMenuEntry("Karten", "Eine Karten-Anwendung mit GoogleMaps",
				new Image(MapApp.class.getResourceAsStream("map.png")), new EventHandler<Event>() {
				@Override
				public void handle(Event event) {
					context.launch(new MapApp());
				}
			});
		
		app.addMenuEntry("Video", "Film ausw�hlen und abspielen",
				new Image(VideoApp.class.getResourceAsStream("video.png")), new EventHandler<Event>() {
				@Override
				public void handle(Event event) {
					context.launch(new VideoApp(videoDirectory));
				}
			});

		app.addMenuEntry("Photo", "Photo ausw�hlen und Bildeigenschaften �ndern",
				new Image(PhotoApp.class.getResourceAsStream("photo.png")), new EventHandler<Event>() {
				@Override
				public void handle(Event event) {
					context.launch(new PhotoApp(photoDirectory));
				}
			});
		context.launch(app);
	}
	
	private void startSnaphsoter(Stage stage) {
		System.out.println("Start Snapshoter");

		MonochromeFlexKitConnector connector = null;
		connector = new MonochromeFlexKitConnector(OperationMode.NORMAL);
		connector.setDitherMatrix(MonochromeFlexKitConnector.FlexModBayerDither);
		connector.dry(!startSnapshoter);
		
		try {
			connector.connect();
		} catch (UnknownHostException e) {
			MessagePane.fatalError(context, "Display connection error", "Could not connect to flexible display.\nUnknown Host");
			e.printStackTrace();
			return;

		} catch (IOException e) {
			MessagePane.fatalError(context, "Display connection error", "Could not connect to flexible display.\nIOException");
			e.printStackTrace();
			return;

		} catch (ConnectorException e) {
			MessagePane.fatalError(context, "Display connection error", "Could not connect to flexible display.\nConnectorException");
			e.printStackTrace();
			return;
		}

		try {
			snapshoter = new StageSnapshoter(connector);
			snapshoter.setStage(stage);
			
		} catch (ConnectorException e) {
			MessagePane.fatalError(context, "Display connection error", "Could not create StageSnapshoter.");
			e.printStackTrace();
			return;
		}
		
		snapshoter.setRefreshTypesForContinuousUpdates(
				snapshoterRefreshTypeForContinuousUpdatePartial, snapshoterRefreshTypeForContinuousUpdateFull);
		snapshoter.setPeriod(snapshoterRefreshPeriod);
		snapshoter.start();
		
		context.setSnapshoter(snapshoter);
	}
	
	private void stopSnapshoter() {
		if(snapshoter != null)
		snapshoter.stop();
	}
	
	private void startDeformationSensor() {
		System.out.println("Start deformation sensor");
		
		try {
			System.out.println("startDeformationSensor=" + startDeformationSensor);
			
			deformationSensor = new DeformationSensor(deformationSensorSerialPort, !startDeformationSensor, 
					learnedDataSetFile, deformationSensorKValue, deformationSensorSmoothing);
		} catch (de.tu_darmstadt.informatik.tk.flexdisp.deformationsensor.sensor.ArduinoException e) {
			e.printStackTrace();
		}
		
		context.setDeformationSensor(deformationSensor);
	}
	
	private void stopDeformationSensor() {
		try {
			if(deformationSensor != null)
				deformationSensor.stop();
		} catch (de.tu_darmstadt.informatik.tk.flexdisp.deformationsensor.sensor.ArduinoException e) {
			e.printStackTrace(); //FIXME
		}
	}
	
	private Properties loadConfig(String configFilePath) throws NumberFormatException, InvalidPropertiesFormatException, FileNotFoundException, IOException {
		if(configFilePath == null || configFilePath.isEmpty()) {
			throw new FileNotFoundException();
		}
		
		File file = new File(configFilePath);
		if (!file.isFile()) {
			throw new FileNotFoundException();
		}
		
		Properties props = loadPropertiesFile(file);
		startDeformationSensor = Boolean.parseBoolean(props.getProperty("deformationsensor", "false"));
		learnedDataSetFile = new File(props.getProperty("deformationsensor-dataset", "C:\\Temp\\dataset.txt"));
		deformationSensorKValue = Integer.parseInt(props.getProperty("deformationsensor-kvalue", "1"));
		deformationSensorSmoothing = Integer.parseInt(props.getProperty("deformationsensor-smoothing", "3"));
		deformationSensorSerialPort = props.getProperty("port", "COM3");
		
		startSnapshoter = Boolean.parseBoolean(props.getProperty("snapshoter", "false"));
		snapshoterRefreshTypeForContinuousUpdatePartial = Integer.parseInt(props.getProperty("snapshoterRefreshTypeForContinuousUpdatePartial", "2"));
		snapshoterRefreshTypeForContinuousUpdateFull = Integer.parseInt(props.getProperty("snapshoterRefreshTypeForContinuousUpdateFull", "9"));
		snapshoterRefreshPeriod = Integer.parseInt(props.getProperty("snapshoterRefreshPeriod", "1000"));
		
		photoDirectory = new File(props.getProperty("photodirectory", "C:\\Temp\\photo\\"));
		videoDirectory = new File(props.getProperty("videodirectory", "C:\\Temp\\video\\"));
		
		return props;
	}
	
	private static Properties loadPropertiesFile(File file) throws InvalidPropertiesFormatException, FileNotFoundException, IOException  {		
		Properties props = new Properties();
		FileInputStream fin = new FileInputStream(file);
		props.loadFromXML(fin);
		fin.close();
		return props;
	}
}
